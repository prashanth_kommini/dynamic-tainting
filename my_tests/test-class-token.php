<?php 
class Greeting{ 
	private $hello = 'Hello '; 
	public $helloFr;

	function sayHello($name){ 
		return $this->hello.$name; 
	}
	function sayHelloFr($name){
		return $this->helloFr.$name;
	}
} 

$greeting = new Greeting;
if($greeting instanceof Greeting)
	print "greeting var is of Greeting type\n";


$tokens = token_get_all(<<< FOOBAR
<?php
class Greeting{ 
	private \$hello = 'Hello '; 
	public \$helloFr;

	function sayHello($name){ 
		return \$this->hello.\$name; 
	}
	function sayHelloFr($name){
		return \$this->helloFr.\$name;
	}
}
\$greeting = new Greeting;
FOOBAR
);

foreach ($tokens as $token) {
    if (is_array($token)) {
        echo "Line {$token[2]}: ", token_name($token[0]), " ('{$token[1]}')", PHP_EOL;
    }
}


echo $greeting->sayHello("Sai");

?> 
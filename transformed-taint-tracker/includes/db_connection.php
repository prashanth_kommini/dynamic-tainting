<?php

require_once 'AspisMain.php';
define('DB_SERVER', 'localhost');
define('DB_USER', 'widget_cms');
define('DB_PASS', 'password');
define('DB_NAME', 'widget_corp');
$connection = attAspis(mysql_connect(DB_SERVER, DB_USER, DB_PASS, true));
mysql_select_db(DB_NAME);
if (mysql_errno()) {
    die(deAspis(AspisPrintGuard(concat2(concat(concat2(concat1('Database connection failed: ', attAspis(mysql_error())), ' ('), attAspis(mysql_errno())), ')'))));
}
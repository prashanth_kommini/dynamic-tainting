<?php

require_once 'AspisMain.php';
if (!(isset($layout_context) && Aspis_isset($layout_context))) {
    $layout_context = array('public', false);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
	<head>
		<title>Widget Corp <?php 
if ($layout_context[0] == 'admin') {
    echo deAspis(AspisPrintGuard(array('Admin', false), 'Echo Stmt', array()));
}
?>
</title>
		<link href="stylesheets/public.css" media="all" rel="stylesheet" type="text/css" />
	</head>
	<body>
    <div id="header">
      <h1>Widget Corp <?php 
if ($layout_context[0] == 'admin') {
    echo deAspis(AspisPrintGuard(array('Admin', false), 'Echo Stmt', array()));
}
?>
</h1>
    </div>

<?php

require_once 'AspisMain.php';
require_once '/usr/local/zend/share/UserServer/transformed-taint-tracker/includes/session.php';
require_once '/usr/local/zend/share/UserServer/transformed-taint-tracker/includes/functions.php';
require_once '/usr/local/zend/share/UserServer/transformed-taint-tracker/includes/validation_functions.php';
require_once '/usr/local/zend/share/UserServer/transformed-taint-tracker/includes/db_connection.php';
include '/usr/local/zend/share/UserServer/transformed-taint-tracker/includes/layouts/header.php';
?>

<?php 
find_selected_page();
if ( (denot_boolean($current_page)))
 {echo deAspis(AspisPrintGuard(array("current_page not set",false)));
};
?>

<?php 
if (isset($_POST[0]['submit']) && Aspis_isset($_POST[0]['submit'])) {
    $id = $current_page[0]['id'];
    $menu_name = $_POST[0]['menu_name'];
    $subject_id = int_cast($_POST[0]['subject_id']);
    $content = $_POST[0]['content'];
    $position = int_cast($_POST[0]['position']);
    $visible = int_cast($_POST[0]['visible']);
    $required_fields = array(array(array('menu_name', false), array('subject_id', false), array('content', false)), false);
    validate_presences($required_fields);
    $fields_with_max_lengths = array(array('menu_name' => array(30, false, false)), false);
    validate_max_lengths($fields_with_max_lengths);
    // echo deAspis(AspisPrintGuard(array('JUST BEFORE EMPTY ERRORS', false), 'Echo Stmt', array()));
    if (empty($errors) || Aspis_empty($errors)) {
        $query = array('UPDATE pages SET ', false);
        $query = concat($query, concat2(concat1('menu_name = \'', $menu_name), '\', '));
        $query = concat($query, concat2(concat1('position = ', $position), ', '));
        $query = concat($query, concat2(concat1('visible = ', $visible), ', '));
        $query = concat($query, concat2(concat1('content = \'', $content), '\' '));
        $query = concat($query, concat2(concat1('WHERE id = ', $id), ' '));
        $query = concat2($query, 'LIMIT 1');
        $result = attAspis(Aspis_mysql_query($query));
        echo deAspis(AspisPrintGuard(concat(concat2(concat1('query is ', $query), ' and results is '), $result)));
        if ($result[0]) {
            arrayAssign($_SESSION[0], deAspis(registerTaint(array('message', false))), addTaint(array('Page updated.', false)));
            redirect_to(concat1('index.php?page=', $id));
        } else {
            arrayAssign($_SESSION[0], deAspis(registerTaint(array('message', false))), addTaint(array('Page update failed.', false)));
        }
    } else {
    }
} else {
}
?>

<?php 
$layout_context = array('public', false);
include '/usr/local/zend/share/UserServer/transformed-taint-tracker/includes/layouts/header.php';
?>

<div id="main">
  <div id="navigation">
    <?php 
echo deAspis(AspisPrintGuard(navigation($current_subject, $current_page), 'Echo Stmt', array('current_subject' => $current_subject, 'current_page' => $current_page)));
?>
  </div>
  <div id="page">
    <?php 
echo deAspis(AspisPrintGuard(message(), 'Echo Stmt', array()));
?>
    <?php 
echo deAspis(AspisPrintGuard(form_errors($errors), 'Echo Stmt', array('errors' => $errors)));
?>
    
    <h2>Edit Page: <?php 
echo deAspis(AspisPrintGuard(AspisKillTaint(Aspis_htmlentities($current_page[0]['menu_name']), 0), 'Echo Stmt', array('current_page' => $current_page)));
?>
</h2>
    <form action="edit_page.php?page=<?php 
echo deAspis(AspisPrintGuard(Aspis_urlencode($current_page[0]['id']), 'Echo Stmt', array('current_page' => $current_page)));
?>
" method="post">
      <p>Menu name:
        <input type="text" name="menu_name" value="<?php 
echo deAspis(AspisPrintGuard(AspisKillTaint(Aspis_htmlentities($current_page[0]['menu_name']), 0), 'Echo Stmt', array('current_page' => $current_page)));
?>
" />
      </p>
      <p>Subject Id:
        <select name="subject_id">
        <?php 
$page_set = find_pages_for_subject($current_page[0]['subject_id']);
$page_count = attAspis(count($page_set[0]));
for ($count = array(1, false); $count[0] <= $page_count[0]; postincr($count)) {
    echo deAspis(AspisPrintGuard(concat2(concat1('<option value="', $count), '"'), 'Echo Stmt', array('count' => $count)));
    if (deAspis($current_page[0]['subject_id']) == $count[0]) {
        echo deAspis(AspisPrintGuard(array(' selected', false), 'Echo Stmt', array()));
    }
    echo deAspis(AspisPrintGuard(concat2(concat1('>', $count), '</option>'), 'Echo Stmt', array('count' => $count)));
}
?>
        </select>
      </p>
      <p>Position:
        <select name="position">
        <?php 
$page_set = find_pages_for_subject($current_page[0]['subject_id'], array(false, false));
$page_count = attAspis(mysql_num_rows($page_set[0]));
for ($count = array(1, false); $count[0] <= $page_count[0]; postincr($count)) {
    echo deAspis(AspisPrintGuard(concat2(concat1('<option value="', $count), '"'), 'Echo Stmt', array('count' => $count)));
    if (deAspis($current_page[0]['position']) == $count[0]) {
        echo deAspis(AspisPrintGuard(array(' selected', false), 'Echo Stmt', array()));
    }
    echo deAspis(AspisPrintGuard(concat2(concat1('>', $count), '</option>'), 'Echo Stmt', array('count' => $count)));
}
?>
        </select>
      </p>
      <p>Visible:
        <input type="radio" name="visible" value="0" <?php 
if (deAspis($current_page[0]['visible']) == 0) {
    echo deAspis(AspisPrintGuard(array('checked', false), 'Echo Stmt', array()));
}
?>
 /> No
        &nbsp;
        <input type="radio" name="visible" value="1" <?php 
if (deAspis($current_page[0]['visible']) == 1) {
    echo deAspis(AspisPrintGuard(array('checked', false), 'Echo Stmt', array()));
}
?>
/> Yes
      </p>
      <p>Content:<br />
        <textarea name="content" rows="20" cols="80"><?php 
echo deAspis(AspisPrintGuard(AspisKillTaint(Aspis_htmlentities($current_page[0]['content']), 0), 'Echo Stmt', array('current_page' => $current_page)));
?>
</textarea>
      </p>
      <input type="submit" name="submit" value="Submit" />
    </form>
    <br />
    <!-- <a href="manage_content.php?page=<?php 
echo deAspis(AspisPrintGuard(Aspis_urlencode($current_page[0]['id']), 'Echo Stmt', array('current_page' => $current_page)));
?>
">Cancel</a>
    &nbsp;
    &nbsp;
    <a href="delete_page.php?page=<?php 
echo deAspis(AspisPrintGuard(Aspis_urlencode($current_page[0]['id']), 'Echo Stmt', array('current_page' => $current_page)));
?>
" onclick="return confirm('Are you sure?');">Delete page</a> -->
    
  </div>
</div>

<?php 
include '/usr/local/zend/share/UserServer/transformed-taint-tracker/includes/layouts/footer.php';
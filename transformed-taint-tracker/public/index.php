<?php

require_once 'AspisMain.php';
require_once '/usr/local/zend/share/UserServer/transformed-taint-tracker/includes/session.php';
require_once '/usr/local/zend/share/UserServer/transformed-taint-tracker/includes/functions.php';
require_once '/usr/local/zend/share/UserServer/transformed-taint-tracker/includes/db_connection.php';
$layout_context = array('public', false);
include '/usr/local/zend/share/UserServer/transformed-taint-tracker/includes/layouts/header.php';
find_selected_page();
?>

<div id="main">
  <div id="navigation">
		<?php 
echo deAspis(AspisPrintGuard(public_navigation($current_subject, $current_page), 'Echo Stmt', array('current_subject' => $current_subject, 'current_page' => $current_page)));
?>
  </div> 
  <div id="page">
		<?php 
if ($current_page[0] && deAspis($current_page[0]['id']) == 1) {
    ?>
			
			<h2><?php 
    echo deAspis(AspisPrintGuard(AspisKillTaint(Aspis_htmlentities($current_page[0]['menu_name']), 0), 'Echo Stmt', array('current_page' => $current_page)));
    ?>
</h2>
			<?php 
    echo deAspis(AspisPrintGuard(Aspis_nl2br(AspisKillTaint(Aspis_htmlentities($current_page[0]['content']), 0)), 'Echo Stmt', array('current_page' => $current_page)));
    ?>
<br />
			<a href="edit_page.php?page=<?php 
    echo deAspis(AspisPrintGuard(Aspis_urlencode($current_page[0]['id']), 'Echo Stmt', array('current_page' => $current_page)));
    ?>
">Edit page
			</a>
			
		<?php 
} elseif ($current_page[0] && deAspis($current_page[0]['id']) == 3) {
    ?>

			<h2>Update Mission</h2>
			Menu name: <?php 
    echo deAspis(AspisPrintGuard(AspisKillTaint(Aspis_htmlentities($current_page[0]['menu_name']), 0), 'Echo Stmt', array('current_page' => $current_page)));
    ?>
<br />
			Subject id: <?php 
    echo deAspis(AspisPrintGuard($current_page[0]['subject_id'], 'Echo Stmt', array('current_page' => $current_page)));
    ?>
<br />			
			Content:<br />
			<div class="view-content">
				<?php 
    echo deAspis(AspisPrintGuard(AspisKillTaint(Aspis_htmlentities($current_page[0]['content']), 0), 'Echo Stmt', array('current_page' => $current_page)));
    ?>
			</div>
			<br />
      		<br />
      		<a href="edit_page.php?subject=<?php 
    echo deAspis(AspisPrintGuard(Aspis_urlencode($current_subject[0]['id']), 'Echo Stmt', array('current_subject' => $current_subject)));
    ?>
">Edit page</a>
      	<?php 
} else {
    ?>
			
			<p>Welcome!</p>
			
		<?php 
}
?>
  </div>
</div>

<?php 
include '/usr/local/zend/share/UserServer/transformed-taint-tracker/includes/layouts/footer.php';
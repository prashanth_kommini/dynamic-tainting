#!/usr/bin/env python

transfomer_script_path = "/home/vkommi2/dynamic-tainting/PHP-Parser/transform/transform.php"

def traverse_and_transform(directory):
	'''
	General function for traversing a local directory. Walks through
	the entire directory, and touches all files with a specified function.
	'''
	for root, dirs, files in os.walk(directory):
		for filename in files:
			transform(os.path.join(root, filename))
		for sub_dir in dirs:
			traverse_and_transform(sub_dir)

def transform(file_to_transform):
	transform_cmd = "" % ()

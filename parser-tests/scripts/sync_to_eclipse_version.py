#!/usr/bin/env python
import sys, fileinput, os, re

eclipse_file = "/root/workspace/ast-translator/src/NewASTCreator/XMLReading.java"

f = open(eclipse_file, 'r+')
text = f.read()
f.close()
text = re.sub('project.at.ac.tuwien.infosys.www.phpparser.*', 'at.ac.tuwien.infosys.www.phpparser.*', text)
text = re.sub('package NewASTCreator;', '', text)

g = open("/root/workspace/ast-translator/buildable-ast-translator/doc/example/XMLReading.java","w+")
g.seek(0)
g.write(text)
g.truncate()
g.close()


# f = open("/root/workspace/ast-translator/buildable-ast-translator/doc/example/test.txt","r")
# lines = f.readlines()
# f.close()



# f = open("/root/workspace/ast-translator/buildable-ast-translator/doc/example/test.txt","w")
# for i, line in enumerate(lines):
# 	sys.stdout.write(line.replace('project.at.ac.tuwien.infosys.www.phpparser.*', 'at.ac.tuwien.infosys.www.phpparser.*'))
# 	if i == 0 or i ==1:
# 		continue
# 	else:
# 		f.write(line)
# f.close()

os.system("subl /root/workspace/ast-translator/buildable-ast-translator/doc/example/XMLReading.java")

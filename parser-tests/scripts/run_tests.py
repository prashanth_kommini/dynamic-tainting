#!/usr/bin/env python

import os, sys, fileinput, re

#	get base dir
# 	construct file path
# 	get ast
# 	get translated ast
# 	compare the two


tests_dir = "/home/vkommi2/dynamic-tainting/parser-tests/"
for test_file in os.listdir(tests_dir):
	if test_file.startswith(sys.argv[1]):
		abs_path_to_testfile = tests_dir + test_file
		break

aspis_home = "/home/vkommi2/dynamic-tainting/aspis_home/"
my_transform_dir = "/home/vkommi2/dynamic-tainting/PHP-Parser/transform/"

aspis_transform_dir = my_transform_dir + "aspis-transformed/" 
transformed_dir = "/home/vkommi2/dynamic-tainting/PHP-Parser/transform/transformed/"
parser_dir = "/home/vkommi2/dynamic-tainting/PHP-Parser/"
aspis_trans_file = "%s%s" % (aspis_transform_dir, test_file) 
my_trans_file = "%s%s" % (transformed_dir, test_file)

untranformed_dump_file = abs_path_to_testfile + ".dump"
aspis_dump_file = aspis_trans_file + ".dump"
my_dump_file = my_trans_file + ".dump"

aspis_cmd = "%saspis -in %s%s -categories %stests/taint_propagation/generic.categories -out %s" % (aspis_home, tests_dir, test_file, aspis_home, aspis_transform_dir)
my_transform_cmd = "php %stransform.php %s%s > %s" % (my_transform_dir, tests_dir, test_file, my_dump_file)
aspis_ast_dump_cmd = "php %smy_test.php %s > %s" % (parser_dir, aspis_trans_file,aspis_dump_file)

diff_dir = my_transform_dir +  "diff-files/" 
diff_file = diff_dir + test_file + ".diff" 
diff_cmd = "diff %s %s > %s" % (aspis_dump_file, my_dump_file, diff_file)

if os.path.isfile(abs_path_to_testfile):
	print "1. File Exists - Success"
	print "Test file: ", abs_path_to_testfile, "..."

	print "2. Running Aspis Transformation on with command %s" % (aspis_cmd)
	os.system(aspis_cmd)
	if os.path.isfile(aspis_trans_file):
		print "Aspis Output file creation - SUCCESS"
		open_aspis_trans_cmd = 	"subl %s" % (aspis_trans_file)
		os.system(open_aspis_trans_cmd)
	else:
		print "Aspis Output file creation - FAILED, Exiting..."
		sys.exit(1)
	
	print "3. Running my_test.php for Aspis transformed file with cmd... %s" % (aspis_ast_dump_cmd)
	os.system(aspis_ast_dump_cmd)
	if os.path.isfile(aspis_dump_file):
		print "Aspis Transform dump file creation - SUCCESS"
		open_aspis_dump_cmd = 	"subl %s" % (aspis_dump_file)
		os.system(open_aspis_dump_cmd)
	else:
		print "Aspis Transform dump file creation - FAILED"
		sys.exit(1)

	# print "4. Running our new transformation with command %s" % (my_transform_cmd)
	# os.system( my_transform_cmd )
	# if os.path.isfile(my_trans_file):
	# 	print "Our Transform Output file created - SUCCESS"
	# else:
	# 	print "Our Transform Output file created - Failed"
	# 	sys.exit(1)

	# print "5. Find Differences"
	# os.system(diff_cmd)
	# if os.path.isfile(diff_file):
	# 	print "Diff file creation - SUCCESS"
	# 	print "6. Opening diff file"
	# 	os.system("subl %s" % (diff_file))
	# else:
	# 	print "Diff file creation - FAILED"
	# 	sys.exit(1)
else:
	print "1. File Exists - Failed"



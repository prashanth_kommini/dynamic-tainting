<?php 
class Greeting{ 
	private $hello; 
	public $helloFr;

	function __construct($hello, $helloFr){
		$this->hello = $hello;
		$this->helloFr = $helloFr;
	}

	function sayHello($name){ 
		return $this->hello.$name; 
	}
	function sayHelloFr($name){
		return $this->helloFr.$name;
	}
} 

$greeting = new Greeting("Hello ", "Bonjour ");
$greeting2 = new Greeting("Hey There ", "Coucou ");
$name = "Sai";
$tmp = "\$greeting->sayHello(\"$name\")";
echo "echoing \$tmp with bracket";
echo {$tmp};
echo "echoing \$tmp";
echo $tmp;

echo $greeting2->sayHelloFr("Sai");
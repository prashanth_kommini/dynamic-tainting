<?php require_once("/usr/local/zend/share/UserServer/taint-tracker/includes/session.php"); ?>
<?php require_once("/usr/local/zend/share/UserServer/taint-tracker/includes/functions.php"); ?>
<!-- <?php require_once("/usr/local/zend/share/UserServer/taint-tracker/includes/validation_functions.php"); ?> -->
<?php require_once("/usr/local/zend/share/UserServer/taint-tracker/includes/db_connection.php"); ?>
<?php include("/usr/local/zend/share/UserServer/taint-tracker/includes/layouts/header.php"); ?>

<?php find_selected_page(); ?>

<?php

	if (!$current_page) {  
		redirect_to("index.php");
	}
?>

<?php
if (isset($_POST['submit'])) {
	// Process the form

	$id = $current_page["id"];
	$menu_name = $_POST["menu_name"];
	$subject_id = (int) $_POST["subject_id"];  
	$content = $_POST["content"];
	$position = (int) $_POST["position"];
	$visible = (int) $_POST["visible"];

	// Perform Update
	$query  = "UPDATE pages SET ";
	$query .= "menu_name = '{$menu_name}', ";
	$query .= "position = {$position}, ";
	$query .= "visible = {$visible}, ";
	$query .= "content = '{$content}' ";
	$query .= "WHERE id = {$id} ";
	$query .= "LIMIT 1";
	$result = mysql_query($query);
	if ($result) {
		// Success
		redirect_to("index.php?page={$id}");
	} 
}
?>

<?php $layout_context = "public"; ?>

<div id="main">
	<div id="navigation">
		<?php echo navigation($current_subject, $current_page); ?>
	</div>
	<div id="page">
		
		<h2>Edit Page: <?php echo $current_page["menu_name"]; ?></h2>
		<form action="edit_page.php?page=<?php echo urlencode($current_page["id"]); ?>" method="post">
			<p>Menu name:
				<input type="text" name="menu_name" value="<?php echo $current_page["menu_name"]; ?>" />
			</p>
			<p>Subject Id:
				<select name="subject_id">
				<?php
					$page_set = find_pages_for_subject($current_page["subject_id"]);
					$page_count = count($page_set);
					for($count=1; $count <= $page_count; $count++) {
						echo "<option value=\"{$count}\"";
						if ($current_page["subject_id"] == $count) {
							echo " selected";
						}
						echo ">{$count}</option>";
					}
				?>
				</select>
			</p>
			<p>Position:
				<select name="position">
				<?php
					$page_set = find_pages_for_subject($current_page["subject_id"], false);
					$page_count = mysql_num_rows($page_set);
					for($count=1; $count <= $page_count; $count++) {
						echo "<option value=\"{$count}\"";
						if ($current_page["position"] == $count) {
							echo " selected";
						}
						echo ">{$count}</option>";
					}
				?>
				</select>
			</p>
			<p>Visible:
				<input type="radio" name="visible" value="0" <?php if ($current_page["visible"] == 0) { echo "checked"; } ?> /> No
				&nbsp;
				<input type="radio" name="visible" value="1" <?php if ($current_page["visible"] == 1) { echo "checked"; } ?>/> Yes
			</p>
			<p>Content:<br />
				<textarea name="content" rows="20" cols="80"><?php echo $current_page["content"]; ?></textarea>
			</p>
			<input type="submit" name="submit" value="Submit" />
		</form>
		<br />    
	</div>
</div>

<?php include("/usr/local/zend/share/UserServer/taint-tracker/includes/layouts/footer.php"); ?>

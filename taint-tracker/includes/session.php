<?php

	session_start();
	// if ( !isset($GLOBALS["pages"])) {
	// 	echo "GLOBALS pages is not set setting again\n";
	// 	$GLOBALS["subjects"] = array();
	// 	$GLOBALS["subjects"][] = array("id"=> 1, "menu_name" => "About Target CMS");
	// 	$GLOBALS["subjects"][] = array("id"=> 2, "menu_name" => "Leave a message");
	// 	$GLOBALS["pages"][] = array("id"=> 1, "subject_id"=>1, "menu_name"=>"Our Mission", "page_content" => "Target CMS is a test application to demonstrate effective DIFT in a web application.");
	// 	$GLOBALS["pages"][] = array("id"=> 2, "subject_id"=>2, "menu_name"=>"Message", "page_content" => "Let us know your comments about our site");
	// }
	// else 
	// 	echo "GLOBALS IS SET";

	function message() {
		if (isset($_SESSION["message"])) {
			$output = "<div class=\"message\">";
			$output .= htmlentities($_SESSION["message"]);
			$output .= "</div>";
			
			// clear message after use
			$_SESSION["message"] = null;
			
			return $output;
		}
	}

	function errors() {
		if (isset($_SESSION["errors"])) {
			$errors = $_SESSION["errors"];
			
			// clear message after use
			$_SESSION["errors"] = null;
			
			return $errors;
		}
	}
	
?>
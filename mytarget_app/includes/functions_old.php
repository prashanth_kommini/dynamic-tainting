<?php

	function redirect_to($new_location) {
	  header("Location: " . $new_location);
	  exit;
	}

	function mysql_prep($string) {
		global $connection;
		
		$escaped_string = mysql_real_escape_string($string);
		return $escaped_string;
	}
	
	function confirm_query($result_set) {
		if (!$result_set) {
			die("Database query failed.");
		}
	}

	function form_errors($errors=array()) {
		$output = "";
		if (!empty($errors)) {
		  $output .= "<div class=\"error\">";
		  $output .= "Please fix the following errors:";
		  $output .= "<ul>";
		  foreach ($errors as $key => $error) {
		    $output .= "<li>";
				$output .= htmlentities($error);
				$output .= "</li>";
		  }
		  $output .= "</ul>";
		  $output .= "</div>";
		}
		return $output;
	}
	
	function find_all_subjects() {		
		$subject_set = $GLOBALS["subjects"];
		// $subject_set[] = array("id"=> 1, "menu_name" => "About Target CMS");
		// $subject_set[] = array("id"=> 2, "menu_name" => "Leave a message");
		// $subject_set[] = array("id"=> 3, "menu_name" => "Edit message");
		return $subject_set;
	}
	
	function find_pages_for_subject($subject_id) {		
		$page_set = array();
		// if($subject_id == 1)
		// {			
		// 	$page_set[] = array("id"=> 1, "subject_id"=>1, "menu_name"=>"Our Mission", "page_content" => "Target CMS is a test application to demonstrate effective DIFT in a web application.");
		// }
		// else if($subject_id == 2)
		// {
		// 	$page_set[] = array("id"=> 2, "subject_id"=>2, "menu_name"=>"Message", "page_content" => "Let us know your comments about our site");
		// }
		// else if($subject_id == 3)
		// {
		// 	$page_set[] = array("id"=> 3, "subject_id"=>3, "menu_name"=>"Update", "page_content" => "Target CMS is a test application to demonstrate effective DIFT in a web application.");
		// }
		$int_id = intval($subject_id)-1;
		$page_set[] = $GLOBALS["pages"][$int_id];
		return $page_set;
	}
	
	function find_all_admins() {
		global $connection;
		
		$query  = "SELECT * ";
		$query .= "FROM admins ";
		$query .= "ORDER BY username ASC";
		$admin_set = mysql_query($query);
		confirm_query($admin_set);
		return $admin_set;
	}
	
	function find_subject_by_id($subject_id) {				
		// if($subject_id == 1)
		// {
		// 	$subject = $GLOBALS[]			
		// }	
		// else if($subject_id == 2)
		// {
		// 	$subject = array("id"=>2, "menu_name"=>"Leave a message");
		// }
		// else if($subject_id == 3)
		// {
		// 	$subject = array("id"=>3, "menu_name"=>"Edit message");
		// }
		// else
		// {
		// 	$subject = null;
		// }
		$int_id = intval($subject_id)-1;
		$subject = $GLOBALS["subjects"][$int_id];
		if($subject != null) {			
			return $subject;
		} else {
			return null;
		}
	}

	function find_page_by_id($page_id) {
		// $page = array();
		// if($page_id == 1)
		// {
		// 	$page = array("id"=>1, "subject_id"=>1, "menu_name"=>"Our Mission", "page_content"=>"Target CMS is a test application to demonstrate effective DIFT in a web application.");
		// }	
		// else if($page_id == 2)
		// {
		// 	$page = array("id"=>2, "subject_id"=>2, "menu_name"=>"Message", "page_content"=>"Let us know your comments about our site");
		// }			
		// else if($page_id == 3)
		// {
		// 	$page = array("id"=>3, "subject_id"=>3, "menu_name"=>"Update", "page_content"=>"");
		// }
		$int_id = intval($page_id)-1;
		$page = $GLOBALS["pages"][$int_id];
		if($page != null) {
			return $page;
		} else {
			return null;
		}
	}
	
	// function find_admin_by_id($admin_id) {
	// 	global $connection;
		
	// 	$safe_admin_id = mysql_real_escape_string($admin_id);
		
	// 	$query  = "SELECT * ";
	// 	$query .= "FROM admins ";
	// 	$query .= "WHERE id = {$safe_admin_id} ";
	// 	$query .= "LIMIT 1";
	// 	$admin_set = mysql_query($query);
	// 	confirm_query($admin_set);
	// 	if($admin = mysql_fetch_assoc($admin_set)) {
	// 		return $admin;
	// 	} else {
	// 		return null;
	// 	}
	// }

	function find_admin_by_username($username) {
		global $connection;
		
		$safe_username = mysql_real_escape_string($username);
		
		$query  = "SELECT * ";
		$query .= "FROM admins ";
		$query .= "WHERE username = '{$safe_username}' ";
		$query .= "LIMIT 1";
		$admin_set = mysql_query($query);
		confirm_query($admin_set);
		if($admin = mysql_fetch_assoc($admin_set)) {
			return $admin;
		} else {
			return null;
		}
	}

	function find_default_page_for_subject($subject_id) {
		$page_set = find_pages_for_subject($subject_id);
		return $page_set[0];
	}
	
	function find_selected_page() {

		global $current_subject;
		global $current_page;
		// echo "VARDUMPING FROM FIND SELECTED PAGE\n";
		// echo $current_page;
		if (isset($_GET["subject"])) {
			echo 'SUBJECT IS SET';
			$current_subject = find_subject_by_id($_GET["subject"]);
			echo "PAGES";
			var_dump($GLOBALS["pages"]);
			// echo "CURRENT SUBJECT";
			// var_dump($current_subject);
			if ($current_subject) {	
				echo "IN CURRENT_PAGE SET WITHIN GET SUBJECT SET";	
				$current_page = find_default_page_for_subject($current_subject["id"]);
				
			} else {
				$current_page = null;
			}
		} elseif (isset($_GET["page"])) {
			echo "PAGE IS SET";
			$current_subject = null;
			$current_page = find_page_by_id($_GET["page"]);
		} else {
			echo "NEITHER PAGE NOR SUBJECT IS SET";
			$current_subject = null;
			$current_page = null;
		}
		echo "VARDUMPING FROM FIND SELECTED PAGE END\n";
		var_dump($current_page);

	}

	// navigation takes 2 arguments
	// - the current subject array or null
	// - the current page array or null
	function navigation($subject_array, $page_array) {
		echo "Current page = ".$page_array["menu_name"]."\n";
		$output = "<ul class=\"subjects\">";
		$subject_set = find_all_subjects();
		foreach($subject_set as $subject) {
			$output .= "<li";
			if ($subject_array && $subject["id"] == $subject_array["id"]) {
				$output .= " class=\"selected\"";
			}
			$output .= ">";
			$output .= "<a href=\"index.php?subject=";
			$output .= urlencode($subject["id"]);
			$output .= "\">";
			$output .= htmlentities($subject["menu_name"]);
			$output .= "</a>";
			
			$page_set = find_pages_for_subject($subject["id"], false);
			$output .= "<ul class=\"pages\">";
			foreach($page_set as $page) {
				$output .= "<li";
				if ($page_array && $page["id"] == $page_array["id"]) {
					$output .= " class=\"selected\"";
				}
				$output .= ">";
				$output .= "<a href=\"index.php?page=";
				$output .= urlencode($page["id"]);
				$output .= "\">";
				$output .= htmlentities($page["menu_name"]);
				$output .= "</a></li>";
			}
			
			$output .= "</ul></li>";
		}
		
		$output .= "</ul>";
		return $output;
	}

	function public_navigation($subject_array, $page_array) {
		// echo "VARDUMPING FROM PAGE NAVIGATION\n";
		// var_dump($page_array);
		$output = "<ul class=\"subjects\">";
		$subject_set = find_all_subjects();
		// $subject = mysql_fetch_assoc($subject_set)
		foreach($subject_set as $subject) {
			$output .= "<li";
			if ($subject_array && $subject["id"] == $subject_array["id"]) {
				$output .= " class=\"selected\"";
			}
			$output .= ">";
			$output .= "<a href=\"index.php?subject=";
			$output .= urlencode($subject["id"]);
			$output .= "\">";
			$output .= htmlentities($subject["menu_name"]);
			$output .= "</a>";
			
			if ($subject_array["id"] == $subject["id"] || 
					$page_array["subject_id"] == $subject["id"]) {
				$page_set = find_pages_for_subject($subject["id"]);

				$output .= "<ul class=\"pages\">";
				foreach($page_set as $page) {
					$output .= "<li";
					if ($page_array && $page["id"] == $page_array["id"]) {
						$output .= " class=\"selected\"";
					}
					$output .= ">";
					$output .= "<a href=\"index.php?page=";
					$output .= urlencode($page["id"]);
					$output .= "\">";
					$output .= htmlentities($page["menu_name"]);
					$output .= "</a></li>";
				}
				$output .= "</ul>";				
			}

			$output .= "</li>"; // end of the subject li
		}		
		$output .= "</ul>";
		return $output;
	}

	function password_encrypt($password) {
  	$hash_format = "$2y$10$";   // Tells PHP to use Blowfish with a "cost" of 10
	  $salt_length = 22; 					// Blowfish salts should be 22-characters or more
	  $salt = generate_salt($salt_length);
	  $format_and_salt = $hash_format . $salt;
	  $hash = crypt($password, $format_and_salt);
		return $hash;
	}
	
	function generate_salt($length) {
	  // Not 100% unique, not 100% random, but good enough for a salt
	  // MD5 returns 32 characters
	  $unique_random_string = md5(uniqid(mt_rand(), true));
	  
		// Valid characters for a salt are [a-zA-Z0-9./]
	  $base64_string = base64_encode($unique_random_string);
	  
		// But not '+' which is valid in base64 encoding
	  $modified_base64_string = str_replace('+', '.', $base64_string);
	  
		// Truncate string to the correct length
	  $salt = substr($modified_base64_string, 0, $length);
	  
		return $salt;
	}
	
	function password_check($password, $existing_hash) {
		// existing hash contains format and salt at start
	  $hash = crypt($password, $existing_hash);
	  if ($hash === $existing_hash) {
	    return true;
	  } else {
	    return false;
	  }
	}

	function attempt_login($username, $password) {
		$admin = find_admin_by_username($username);
		if ($admin) {
			// found admin, now check password
			if (password_check($password, $admin["hashed_password"])) {
				// password matches
				return $admin;
			} else {
				// password does not match
				return false;
			}
		} else {
			// admin not found
			return false;
		}
	}

	function logged_in() {
		return isset($_SESSION['admin_id']);
	}
	
	function confirm_logged_in() {
		if (!logged_in()) {
			redirect_to("login.php");
		}
	}

?>


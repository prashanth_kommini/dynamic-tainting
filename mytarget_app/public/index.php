<?php require_once("/usr/local/zend/share/UserServer/mytarget_app/includes/session.php"); ?>
<?php require_once("/usr/local/zend/share/UserServer/mytarget_app/includes/functions.php"); ?>
<?php require_once("/usr/local/zend/share/UserServer/mytarget_app/includes/db_connection.php"); ?>
<?php $layout_context = "public"; ?>
<?php include("/usr/local/zend/share/UserServer/mytarget_app/includes/layouts/header.php"); ?>
<?php 
// echo "vardumping pages";
// var_dump($GLOBALS["pages"]);
find_selected_page();
?>

<div id="main">
  <div id="navigation">
		<?php echo public_navigation($current_subject, $current_page); ?>
  </div> 
  <div id="page">
		<?php if ($current_page && $current_page["id"]==1) { ?>
			
			<h2><?php echo htmlentities($current_page["menu_name"]); ?></h2>
			<?php echo nl2br(htmlentities($current_page["content"])); ?><br />
			<a href="edit_page.php?page=<?php echo urlencode($current_page['id']); ?>">Edit page</a>
			
		<?php } 
		     elseif ($current_page && $current_page["id"]==3) { ?>

			<h2>Update Mission</h2>
			Menu name: <?php echo htmlentities($current_page["menu_name"]); ?><br />
			Subject id: <?php echo $current_page["subject_id"]; ?><br />			
			Content:<br />
			<div class="view-content">
				<?php echo htmlentities($current_page["content"]); ?>
			</div>
			<br />
      		<br />
      		<a href="edit_page.php?subject=<?php echo urlencode($current_subject['id']); ?>">Edit page</a>
      	<?php } 

			 else { ?>
			
			<p>Welcome!</p>
			
		<?php }?>
  </div>
</div>

<?php include("/usr/local/zend/share/UserServer/mytarget_app/includes/layouts/footer.php"); ?>

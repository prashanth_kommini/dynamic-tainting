<?php require_once("/usr/local/zend/share/UserServer/mytarget_app/includes/session.php"); ?>
<?php require_once("/usr/local/zend/share/UserServer/mytarget_app/includes/functions.php"); ?>
<?php require_once("/usr/local/zend/share/UserServer/mytarget_app/includes/validation_functions.php"); ?>
<?php require_once("/usr/local/zend/share/UserServer/mytarget_app/includes/db_connection.php"); ?>

<?php find_selected_page(); ?>

<?php
  echo "VARDUMPING CURRENT PAGE";
  var_dump($current_page);
  if (!$current_page) {  
    // redirect_to("index.php");
    echo "current_page not set";
  }
?>

<?php
if (isset($_POST['submit'])) {
  echo 'INSIDE POSTsubmit true';
  // Process the form
  // var_dump($current_page);

  $id = $current_page["id"];
  $menu_name = $_POST["menu_name"];
  $subject_id = (int) $_POST["subject_id"];  
  $content = $_POST["content"];
  $position = (int) $_POST["position"];
  $visible = (int) $_POST["visible"];
  // echo "VARDUMPING FROM if(isset)";
  // echo "PAGE CONTENT IS: ". $page_content;
  // var_dump($id);
  // var_dump($subject_id);
  // echo "menuname is " . $menu_name;
  // validations
  $required_fields = array("menu_name", "subject_id", "content");
  validate_presences($required_fields);
  
  $fields_with_max_lengths = array("menu_name" => 30);
  validate_max_lengths($fields_with_max_lengths);
  echo 'JUST BEFORE EMPTY ERRORS';
  if (empty($errors)) {
    
    // Perform Update
    $query  = "UPDATE pages SET ";
    $query .= "menu_name = '{$menu_name}', ";
    $query .= "position = {$position}, ";
    $query .= "visible = {$visible}, ";
    $query .= "content = '{$content}' ";
    $query .= "WHERE id = {$id} ";
    $query .= "LIMIT 1";
    $result = mysql_query($query);

    echo "query is $query and results is $result";
    if ($result) {
      // Success
      $_SESSION["message"] = "Page updated.";
      redirect_to("index.php?page={$id}");
    } else {
      // Failure
      $_SESSION["message"] = "Page update failed.";
    }
  
  }
  else{
    echo 'THERE ARE ERRORS: edit_page.php';
  }
  echo 'JUST AFTER EMPTY ERRORS';


} else {
  // This is probably a GET request
  
} // end: if (isset($_POST['submit']))

?>

<?php $layout_context = "public"; ?>
<?php include("/usr/local/zend/share/UserServer/mytarget_app/includes/layouts/header.php"); ?>

<div id="main">
  <div id="navigation">
    <?php echo navigation($current_subject, $current_page); ?>
  </div>
  <div id="page">
    <?php echo message(); ?>
    <?php echo form_errors($errors); ?>
    
    <h2>Edit Page: <?php echo htmlentities($current_page["menu_name"]); ?></h2>
    <form action="edit_page.php?page=<?php echo urlencode($current_page["id"]); ?>" method="post">
      <p>Menu name:
        <input type="text" name="menu_name" value="<?php echo htmlentities($current_page["menu_name"]); ?>" />
      </p>
      <p>Subject Id:
        <select name="subject_id">
        <?php
          $page_set = find_pages_for_subject($current_page["subject_id"]);
          $page_count = count($page_set);
          for($count=1; $count <= $page_count; $count++) {
            echo "<option value=\"{$count}\"";
            if ($current_page["subject_id"] == $count) {
              echo " selected";
            }
            echo ">{$count}</option>";
          }
        ?>
        </select>
      </p>
      <p>Position:
        <select name="position">
        <?php
          $page_set = find_pages_for_subject($current_page["subject_id"], false);
          $page_count = mysql_num_rows($page_set);
          for($count=1; $count <= $page_count; $count++) {
            echo "<option value=\"{$count}\"";
            if ($current_page["position"] == $count) {
              echo " selected";
            }
            echo ">{$count}</option>";
          }
        ?>
        </select>
      </p>
      <p>Visible:
        <input type="radio" name="visible" value="0" <?php if ($current_page["visible"] == 0) { echo "checked"; } ?> /> No
        &nbsp;
        <input type="radio" name="visible" value="1" <?php if ($current_page["visible"] == 1) { echo "checked"; } ?>/> Yes
      </p>
      <p>Content:<br />
        <textarea name="content" rows="20" cols="80"><?php echo htmlentities($current_page["content"]); ?></textarea>
      </p>
      <input type="submit" name="submit" value="Submit" />
    </form>
    <br />
    <!-- <a href="manage_content.php?page=<?php echo urlencode($current_page["id"]); ?>">Cancel</a>
    &nbsp;
    &nbsp;
    <a href="delete_page.php?page=<?php echo urlencode($current_page["id"]); ?>" onclick="return confirm('Are you sure?');">Delete page</a> -->
    
  </div>
</div>

<?php include("/usr/local/zend/share/UserServer/mytarget_app/includes/layouts/footer.php"); ?>

<?php

namespace PhpParser\NodeVisitor;
use PhpParser\Node;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;

class NodeFinder extends NodeVisitorAbstract
{
	public $varsList = array();
	public function enterNode(Node $node){
		if($node instanceof Node\Expr\Variable){
			if(!in_array($node, $this->varsList)){
				$this->varsList[] = $node;
			}
		}
	}

	public function getVarsArray(){
		return $this->varsList;
	}
}

<?php

namespace PhpParser\NodeVisitor;
use PhpParser\Node;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;

class MyNodeVisitor extends NodeVisitorAbstract
{


	static public $traversed = 0;
	static function traverse(array $nodes) {
		$nodes = self::traverseArray($nodes, NULL, NULL, true);
		echo "Traversed: " . self::$traversed;
		return $nodes;
	}

	static function traverseNode(Node &$node, $parent = NULL, $isArrayPart, $attrName = NULL, $indexIntoArr) {
		self::$traversed++;
		$retArray = array();

		foreach ($node->getSubNodeNames() as $name) {
			$subNode =& $node->$name;

			if (is_array($subNode)) {
				$subNode = self::traverseArray($subNode, $node, $name);
			} 
			elseif ($subNode instanceof Node) {
				// echo self::$traversed . ". PARENT: " . get_class($parent) . "\nNODE: " . get_class($node) . "\nSUBNODE: " . get_class($subNode) . "\n\n";
				$subNode = self::traverseNode($subNode, $node, false, NULL, -100);
			}
		}
		
		if($node instanceof Node\Stmt\Echo_){
			echo "Found Echo Stmt\n";
			$AspisPrintGuard =& $node->exprs[0]->args[0]->value;
			$AspisPrintGuardArgs =& $AspisPrintGuard->args;

			// $varsArray = array($AspisPrintGuardArgs[0]->value);
			$traverser = new NodeTraverser;
			$nodeFinder = new NodeFinder;
			$traverser->addVisitor($nodeFinder);
			$stmts = $traverser->traverse($node->exprs);
			$varsArray = $nodeFinder->getVarsArray();

			$echoStringNode = self::createScalarStringNode("Echo Stmt");
			$echoStringArgNode = self::createArgNode($echoStringNode, false, false);
			$AspisPrintGuardArgs[] = $echoStringArgNode;

			$arrayItemNodes = array();
			foreach($varsArray as $var){
				$arrayItemNodes[] = self::createArrayItemNode(self::createScalarStringNode($var->name), $var, false);
			}
			$arrayArgNode = self::createArgNode(self::createArrayNode($arrayItemNodes), false, false);
			$AspisPrintGuardArgs[] = $arrayArgNode;
		}


		$count = count($retArray);
		if($count > 1){
			return $retArray;
		}
		else{
			return $node;           
		}
	}

	protected function traverseArray(array &$nodes, $parent = NULL, $name = NULL) {

		echo "Traversing Array of Nodes. Parent: ". get_class($parent) . " $name\n";
		foreach ($nodes as $i => &$node) {
			if (is_array($node)) {
				$nodes = self::traverseArray($node, $parent, $name);
			} elseif ($node instanceof Node) {
				$ret = self::traverseNode($node, $parent, true, $name, $i);
				if( is_array($ret) and count($ret) > 1){
					echo "you returned an array smart guy!! VARDUMPING RET\n";
					// var_dump($ret);
					array_splice($nodes, $i, 1, $ret);
				}
				else{
					echo "you returned a single node\n";
					$node = $ret;
				}
			}
		}
		return $nodes;
	}

	public function beforeTraverse(array $nodes){
		$nodes = self::traverse($nodes);
		return $nodes;	
	}
	

	static function createRetStmtNode($expr, $LineNo){
		return $retStmt = new Node\Stmt\Return_($expr, array('startLine'=>$LineNo, 'endLine'=>$LineNo));
	}

	static function createFuncStmtNode($byRef, $name, $params, $returnType, $stmts){
		$funcDefNode = new Node\Stmt\Function_($name, array('byRef' => $byRef, 'params' => $params, 'returnType' => $returnType, 'stmts' => $stmts), array());
		return $funcDefNode;
	}

	static function createArrayNode($items, $attributes = array()){
		$arrayNode = new Node\Expr\Array_($items, $attributes);
		return $arrayNode;
	}

	static function createArrayItemNode($key, $value, $byRef){
		$arrayItemNode = new Node\Expr\ArrayItem($value, $key, $byRef);                 
		return $arrayItemNode;
	}

	static function createScalarStringNode($value){
		$scalarStringNode = new Node\Scalar\String_($value);
		if($scalarStringNode == NULL){
			echo 'scalarStringNode is null, What the hell are you doing?';
		}
		return $scalarStringNode;
	}

	static function createVariableNode($name){
		$varNode = new Node\Expr\Variable($name);
		return $varNode;
	}

	static function createArrayDimFetchNode($var, $dim){
		$arrayDimNode = new Node\Expr\ArrayDimFetch($var, $dim);
		return $arrayDimNode;
	}

	static function createAssignmentStmtNode($var, $expr){
		$assignStmtNode = new Node\Expr\Assign($var, $expr);
		return $assignStmtNode;
	}

	static function createGlobalStmtNode($vars){
		$globalStmtNode = new Node\Stmt\Global_($vars);
		return $globalStmtNode;
	}

	static function createParamNode($name, $default, $type, $byRef, $variadic){
		$paramNode = new Node\Param($name, $default, $type, $byRef, $variadic);
		return $paramNode;
	}

	static function createArgNode($value, $byRef, $unpack){
		$argNode = new Node\Arg($value, $byRef, $unpack);
		return $argNode;
	}
}

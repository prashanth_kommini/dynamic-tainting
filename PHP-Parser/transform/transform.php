<?php

require "/home/vkommi2/dynamic-tainting/PHP-Parser/lib/bootstrap.php";
use PhpParser\ParserFactory;
use PhpParser\PrettyPrinter;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitor\MyNodeVisitor;


function startsWith($haystack, $needle) {
	// search backwards starting from haystack length characters from the end
	return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith($haystack, $needle) {
	// search forward starting from end minus needle length characters
	return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}


$parser = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);
$traverser = new NodeTraverser;
$prettyPrinter = new PrettyPrinter\Standard;
$traverser->addVisitor(new MyNodeVisitor);

$outDir = "./transformed/";

$opts  = "";
$opts .= "f:"; 
$opts .= "p:";
$opts .= "d:" ;
$options = getopt($opts);
var_dump($options);

if( isset($options["p"]) and isset($options["d"])){
	// $fileName = $options["f"];
	$isProject = $options["p"];
	$projectDir = $options["d"];

	echo "isProject: $isProject\n";
	echo "projectDir: $projectDir\n";
}
else if(isset($options["f"])){

}
else{
	echo "Usage: php transform.php\n-f </file/to/be/transformed>\n-p <\"y\"-project, \"n\"-file>\n-d </project/dir> (optional only if p=y)\n";
	exit(1);
}

// $code = fread($file, filesize($argv[1]) );

if( $isProject == "y" and isset($projectDir) ){

	$rootDir = $projectDir;
	$rootOutDir = "/home/vkommi2/dynamic-tainting/PHP-Parser/transform/transformed/". "transformed-".basename($rootDir)."/";
	echo "rootDir: $rootDir\n";
	echo "rootOutDir: $rootOutDir\n";
	$iter = new RecursiveIteratorIterator(
		new RecursiveDirectoryIterator($rootDir, RecursiveDirectoryIterator::SKIP_DOTS),
		RecursiveIteratorIterator::SELF_FIRST,
		RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied"
	);
	mkdir($rootOutDir, 0777,true);	

	foreach ($iter as $path => $dir) {
		echo "Orig Path: $path\n";
		echo "Orig Dir: $dir\n";
		$newDir = str_replace($rootDir, $rootOutDir, $dir);
		$newPath = str_replace($rootDir, $rootOutDir, $path);
		if ($dir->isDir()) {
			mkdir($newPath, 0777, true);
			if(file_exists($newDir)){
				echo "DIR CREATION SUCCESS: $newPath\n";
			}
			else{
				echo "DIR CREATION FAILURE: $newPath\n";
			}
		}
		else if(is_file($dir)){
			if(endsWith($path, ".php")){
				echo "Its a PHP file";
				try {
					$code = file_get_contents($path);
					// parse
					$stmts = $parser->parse($code);
					// traverse and tranform
					$stmts = $traverser->traverse($stmts);
					// var_dump($stmts);
					// replace new code
					$code = $prettyPrinter->prettyPrintFile($stmts);
					// Copy to file
					// file_put_contents( $outDir . basename($fileName), $code);
					file_put_contents( $newPath, $code);

				}catch (PhpParser\Error $e) {
					echo 'Parse Error: ', $e->getMessage();
				}
			}
			else{
				echo "NOT A PHP File: $path\n";
				$code = file_get_contents($path);
				file_put_contents( $newPath, $code);
			}
			// $content = file_get_contents($path);
			if(file_exists($newPath)){
				echo "FILE CREATION SUCCESS: $newPath\n";
			}
			else{
				echo "FILE CREATION FAILURE: $newPath\n";
			}
		}
	}
}

function echo_memory_usage() { 
	$mem_usage = memory_get_usage(true);
	if ($mem_usage < 1024) 
		echo $mem_usage." bytes"; 
	elseif ($mem_usage < 1048576) 
		echo round($mem_usage/1024,2)." kilobytes"; 
	else 
		echo round($mem_usage/1048576,2)." megabytes"; 
		
	echo "<br/>"; 
}
$path = "/usr/local/zend/share/UserServer/wordpress/php_functions.txt";
$a = file_get_contents($path);
echo $a;w
echo_memory_usage();




				// try {
				// 	$code = file_get_contents($fileName);
				// 	// parse
				// 	$stmts = $parser->parse($code);
				// 	// traverse and tranform
				// 	$stmts = $traverser->traverse($stmts);
				// 	var_dump($stmts);
				// 	// replace new code
				// 	$code = $prettyPrinter->prettyPrintFile($stmts);
				// 	// Copy to file
				// 	file_put_contents( $outDir . basename($fileName), $code);	
				// }catch (PhpParser\Error $e) {
				// 	echo 'Parse Error: ', $e->getMessage();
				// }




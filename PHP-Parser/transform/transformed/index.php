<?php

require_once 'AspisMain.php';
$flag = array(true, false);
$xmlns_url = array('http://www.w3.org/1999/xhtml', false);
$html_open = concat2(concat1('<html xmlns="', $xmlns_url), '" dir="ltr" lang="en-US">');
$profile_url = array('http://gmpg.org/xfn/11', false);
$head = concat2(concat1('<head profile="', $profile_url), '">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title> Testing</title>

<link rel=\'index\' title=\'Testing\' href=\'http://localhost/misc/wordpress\' />
<meta name="generator" content="WordPress 2.9.2" />
</head>');
$div_header = array('<hr />

<div id="header" role="banner">
	<div id="headerimg">
		<div class="description">Just another WordPress weblog</div>
	</div>
</div>', false);
$div_content = array('<hr />

<div id="content" class="narrowcolumn" role="main">
	<div class="post-1 post entry category-uncategorized" id="post-1">
		<small>April 10th, 2016 <!-- by admin --></small>
		<div class="entry">
			<p>Welcome to WordPress. This is your first post. Edit or delete it, then start blogging!</p>
		</div>
	</div>
	
	<div class="navigation">
		<div class="alignleft"></div>
		<div class="alignright"></div>
	</div>

</div>', false);
$div_footer = array('<hr />

<div id="footer" role="contentinfo">
	<p>
		Testing is proudly powered by
		<a href="http://wordpress.org/">WordPress</a>

	</p>
</div>
', false);
$div_page = concat2(concat(concat(concat1('<div id="page">', $div_header), $div_content), $div_footer), '</div>');
$body = concat2(concat1('<body class="home blog logged-in">', $div_page), '</body>');
if ($flag[0]) {
    echo deAspis(AspisPrintGuard(concat($head, $body), 'Echo Stmt', array('head' => $head, 'body' => $body)));
} else {
    echo deAspis(AspisPrintGuard(array('Flag is False
', false), 'Echo Stmt', array()));
}
?>


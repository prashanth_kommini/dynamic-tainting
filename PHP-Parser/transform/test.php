<?php

$rootDir = '/usr/local/zend/share/UserServer/taint-tracker/';
$rootOutDir = "/home/vkommi2/dynamic-tainting/PHP-Parser/transform/transformed/". "transformed-".basename($rootDir)."/";

$iter = new RecursiveIteratorIterator(
	new RecursiveDirectoryIterator($rootDir, RecursiveDirectoryIterator::SKIP_DOTS),
	RecursiveIteratorIterator::SELF_FIRST,
	RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied"
);
mkdir($rootOutDir, 0777,true);

foreach ($iter as $path => $dir) {
	echo "Orig Path: $path\n";
	echo "Orig Dir: $dir\n";
	$newDir = str_replace($rootDir, $rootOutDir, $dir);
	$newPath = str_replace($rootDir, $rootOutDir, $path);
	if ($dir->isDir()) {
		mkdir($newPath, 0777, true);
		if(file_exists($newDir)){
			echo "DIR CREATION SUCCESS: $newPath\n";
		}
		else{
			echo "DIR CREATION FAILURE: $newPath\n";
		}
	}
	else if(is_file($dir)){
		if(endsWith($path, ".php")){
			echo "Its a PHP file";
		}
		else
			echo "NOT A PHP File: $path\n";
		$content = file_get_contents($path);
		file_put_contents( $newPath, $content);
		if(file_exists($newPath)){
			echo "FILE CREATION SUCCESS: $newPath\n";
		}
		else{
			echo "FILE CREATION FAILURE: $newPath\n";
		}
	}
}


function startsWith($haystack, $needle) {
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith($haystack, $needle) {
    // search forward starting from end minus needle length characters
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}
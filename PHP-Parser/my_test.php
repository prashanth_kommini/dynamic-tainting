<?php

require "/home/vkommi2/dynamic-tainting/PHP-Parser/lib/bootstrap.php";
use PhpParser\PrettyPrinter;


$file = fopen($argv[1], "r");
$code = fread($file, filesize($argv[1]) );
// echo 'parsing'. $code;
fclose($file);

$parser = (new PhpParser\ParserFactory)->create(PhpParser\ParserFactory::PREFER_PHP7);
$serializer = new PhpParser\Serializer\XML;
$prettyPrinter = new PrettyPrinter\Standard;

try {
    $stmts = $parser->parse($code);
    var_dump($stmts);
    // echo $serializer->serialize($stmts);
  	// echo $prettyPrinter->prettyPrintFile($stmts);

} catch (PhpParser\Error $e) {
    echo 'Parse Error: ', $e->getMessage();
}	
<?php require_once('AspisMain.php'); ?>    <div id="footer">Copyright <?php
echo deAspis(AspisPrintGuard(attAspis(date(("Y")))));
;
?>, Widget Corp</div>

	</body>
</html>
<?php 	if ( ((isset($connection) && Aspis_isset( $connection))))
 		{
 			mysql_close($connection[0]);
		};
?>
<?php

// Returns instantanious memory usage
function get_memory_usage() { 
    $mem_usage = memory_get_usage(true);
    $ret_string = "";
    if ($mem_usage < 1024) 
        $ret_string .= strval($mem_usage) ." bytes"; 
    elseif ($mem_usage < 1048576) 
        $ret_string .= strval(round($mem_usage/1024,2)) ." kilobytes"; 
    else 
        $ret_string .= strval(round($mem_usage/1048576,2))." megabytes"; 
    
    return $ret_string;
}

$log_file = "/usr/local/zend/share/UserServer/performance_transformed.log";

// Memory Usage
$mem_usage_str = "Memory Used for Request was: " . get_memory_usage() . "\n";
file_put_contents($log_file, $mem_usage_str, FILE_APPEND | LOCK_EX);

// Time Usage
$timestamp_micro = microtime(true);
$time_diff = $timestamp_micro - $_SERVER[0]["REQUEST_TIME_FLOAT"][0];
$time_secs = round($time_diff, 4);
$time_usage_str =  "Time for Request was: $time_secs seconds\n";
file_put_contents($log_file, $time_usage_str, FILE_APPEND | LOCK_EX);
?>
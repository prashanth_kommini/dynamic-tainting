<?php require_once('AspisMain.php'); ?><?php
$errors = array(array(),false);
function fieldname_as_text ( $fieldname ) {
$fieldname = Aspis_str_replace(array("_",false),array(" ",false),$fieldname);
$fieldname = attAspis(ucfirst($fieldname[0]));
return $fieldname;
 }
function has_presence ( $value ) {
return array(((isset($value) && Aspis_isset( $value))) && ($value[0] !== ("")),false);
 }
function validate_presences ( $required_fields ) {
global $errors;
foreach ( $required_fields[0] as $field  )
{$value = Aspis_trim(attachAspis($_POST,$field[0]));
if ( (denot_boolean(has_presence($value))))
 {arrayAssign($errors[0],deAspis(registerTaint($field)),addTaint(concat2(fieldname_as_text($field)," can't be blank")));
}} }
function has_max_length ( $value,$max ) {
return array(strlen($value[0]) <= $max[0],false);
 }
function validate_max_lengths ( $fields_with_max_lengths ) {
global $errors;
foreach ( $fields_with_max_lengths[0] as $field =>$max )
{restoreTaint($field,$max);
{$value = Aspis_trim(attachAspis($_POST,$field[0]));
if ( (denot_boolean(has_max_length($value,$max))))
 {arrayAssign($errors[0],deAspis(registerTaint($field)),addTaint(concat2(fieldname_as_text($field)," is too long")));
}}} }
function has_inclusion_in ( $value,$set ) {
return Aspis_in_array($value,$set);
 }
;

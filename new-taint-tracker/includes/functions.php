<?php require_once('AspisMain.php'); ?><?php
function redirect_to ( $new_location ) {
header((deconcat1("Location: ",$new_location)));
exit();
 }
function mysql_prep ( $string ) {
global $connection;
$escaped_string = $string;
return $escaped_string;
 }
function confirm_query ( $result_set ) {
if ( (denot_boolean($result_set)))
 {exit(deAspis(AspisPrintGuard(array("Database query failed.",false))));
} }
function form_errors ( $errors = array(array(),false) ) {
$output = array("",false);
if ( (!((empty($errors) || Aspis_empty( $errors)))))
 {$output = concat2($output,"<div class=\"error\">");
$output = concat2($output,"Please fix the following errors:");
$output = concat2($output,"<ul>");
foreach ( $errors[0] as $key =>$error )
{restoreTaint($key,$error);
{$output = concat2($output,"<li>");
$output = concat($output,$error);
$output = concat2($output,"</li>");
}}$output = concat2($output,"</ul>");
$output = concat2($output,"</div>");
}return $output;
 }
function find_all_subjects (  ) {
global $connection;
$query = array("SELECT * ",false);
$query = concat2($query,"FROM subjects ");
$query = concat2($query,"WHERE visible = 1 ");
$query = concat2($query,"ORDER BY position ASC");
$subject_set = attAspis(mysql_query($query[0]));
confirm_query($subject_set);
return $subject_set;
 }
function find_pages_for_subject ( $subject_id ) {
global $connection;
$safe_subject_id = $subject_id;
$query = array("SELECT * ",false);
$query = concat2($query,"FROM pages ");
$query = concat($query,concat2(concat1("WHERE subject_id = ",$safe_subject_id)," "));
$query = concat2($query,"AND visible = 1 ");
$query = concat2($query,"ORDER BY position ASC");
$page_set = attAspis(mysql_query($query[0]));
confirm_query($page_set);
return $page_set;
 }
function find_all_admins (  ) {
global $connection;
$query = array("SELECT * ",false);
$query = concat2($query,"FROM admins ");
$query = concat2($query,"ORDER BY username ASC");
$admin_set = attAspis(mysql_query($query[0]));
confirm_query($admin_set);
return $admin_set;
 }
function find_subject_by_id ( $subject_id ) {
global $connection;
$safe_subject_id = $subject_id;
$query = array("SELECT * ",false);
$query = concat2($query,"FROM subjects ");
$query = concat($query,concat2(concat1("WHERE id = ",$safe_subject_id)," "));
$query = concat2($query,"LIMIT 1");
$subject_set = attAspis(mysql_query($query[0]));
confirm_query($subject_set);
if ( deAspis($subject = attAspisRC(mysql_fetch_assoc($subject_set[0]))))
 {return $subject;
}else 
{{return array(null,false);
}} }
function find_page_by_id ( $page_id ) {
global $connection;
$safe_page_id = $page_id;
$query = array("SELECT * ",false);
$query = concat2($query,"FROM pages ");
$query = concat($query,concat2(concat1("WHERE id = ",$safe_page_id)," "));
$query = concat2($query,"LIMIT 1");
$page_set = attAspis(mysql_query($query[0]));
confirm_query($page_set);
if ( deAspis($page = attAspisRC(mysql_fetch_assoc($page_set[0]))))
 {return $page;
}else 
{{return array(null,false);
}} }
function find_default_page_for_subject ( $subject_id ) {
$page_set = find_pages_for_subject($subject_id);
if ( deAspis($first_page = attAspisRC(mysql_fetch_assoc($page_set[0]))))
 {return $first_page;
}else 
{{return array(null,false);
}} }
function find_selected_page (  ) {
global $current_subject;
global $current_page;
if ( ((isset($_GET[0][("subject")]) && Aspis_isset( $_GET [0][("subject")]))))
 {$current_subject = find_subject_by_id($_GET[0]["subject"]);
if ( $current_subject[0])
 {$current_page = find_default_page_for_subject($current_subject[0]["id"]);
}else 
{{$current_page = array(null,false);
}}}elseif ( ((isset($_GET[0][("page")]) && Aspis_isset( $_GET [0][("page")]))))
 {$current_subject = array(null,false);
$current_page = find_page_by_id($_GET[0]["page"]);
}else 
{{$current_subject = array(null,false);
$current_page = array(null,false);
}} }
function navigation ( $subject_array,$page_array ) {
$output = array("<ul class=\"subjects\">",false);
$subject_set = find_all_subjects(array(false,false));
while ( deAspis($subject = attAspisRC(mysql_fetch_assoc($subject_set[0]))) )
{$output = concat2($output,"<li");
if ( ($subject_array[0] && (deAspis($subject[0]["id"]) == deAspis($subject_array[0]["id"]))))
 {$output = concat2($output," class=\"selected\"");
}$output = concat2($output,">");
$output = concat2($output,"<a href=\"manage_content.php?subject=");
$output = concat($output,Aspis_urlencode($subject[0]["id"]));
$output = concat2($output,"\">");
$output = concat($output,$subject[0]["menu_name"]);
$output = concat2($output,"</a>");
$page_set = find_pages_for_subject($subject[0]["id"],array(false,false));
$output = concat2($output,"<ul class=\"pages\">");
while ( deAspis($page = attAspisRC(mysql_fetch_assoc($page_set[0]))) )
{$output = concat2($output,"<li");
if ( ($page_array[0] && (deAspis($page[0]["id"]) == deAspis($page_array[0]["id"]))))
 {$output = concat2($output," class=\"selected\"");
}$output = concat2($output,">");
$output = concat2($output,"<a href=\"manage_content.php?page=");
$output = concat($output,Aspis_urlencode($page[0]["id"]));
$output = concat2($output,"\">");
$output = concat($output,$page[0]["menu_name"]);
$output = concat2($output,"</a></li>");
}mysql_free_result($page_set[0]);
$output = concat2($output,"</ul></li>");
}mysql_free_result($subject_set[0]);
$output = concat2($output,"</ul>");
return $output;
 }
function public_navigation ( $subject_array,$page_array ) {
$output = array("<ul class=\"subjects\">",false);
$subject_set = find_all_subjects();
while ( deAspis($subject = attAspisRC(mysql_fetch_assoc($subject_set[0]))) )
{$output = concat2($output,"<li");
if ( ($subject_array[0] && (deAspis($subject[0]["id"]) == deAspis($subject_array[0]["id"]))))
 {$output = concat2($output," class=\"selected\"");
}$output = concat2($output,">");
$output = concat2($output,"<a href=\"index.php?subject=");
$output = concat($output,Aspis_urlencode($subject[0]["id"]));
$output = concat2($output,"\">");
$output = concat($output,$subject[0]["menu_name"]);
$output = concat2($output,"</a>");
if ( ((deAspis($subject_array[0]["id"]) == deAspis($subject[0]["id"])) || (deAspis($page_array[0]["subject_id"]) == deAspis($subject[0]["id"]))))
 {$page_set = find_pages_for_subject($subject[0]["id"]);
$output = concat2($output,"<ul class=\"pages\">");
while ( deAspis($page = attAspisRC(mysql_fetch_assoc($page_set[0]))) )
{$output = concat2($output,"<li");
if ( ($page_array[0] && (deAspis($page[0]["id"]) == deAspis($page_array[0]["id"]))))
 {$output = concat2($output," class=\"selected\"");
}$output = concat2($output,">");
$output = concat2($output,"<a href=\"index.php?page=");
$output = concat($output,Aspis_urlencode($page[0]["id"]));
$output = concat2($output,"\">");
$output = concat($output,$page[0]["menu_name"]);
$output = concat2($output,"</a></li>");
}$output = concat2($output,"</ul>");
mysql_free_result($page_set[0]);
}$output = concat2($output,"</li>");
}mysql_free_result($subject_set[0]);
$output = concat2($output,"</ul>");
return $output;
 }
function password_encrypt ( $password ) {
$hash_format = array("$2y$10$",false);
$salt_length = array(22,false);
$salt = generate_salt($salt_length);
$format_and_salt = concat($hash_format,$salt);
$hash = Aspis_crypt($password,$format_and_salt);
return $hash;
 }
function generate_salt ( $length ) {
$unique_random_string = attAspis(md5(uniqid(deAspisRC(attAspis(mt_rand())),true)));
$base64_string = Aspis_base64_encode($unique_random_string);
$modified_base64_string = Aspis_str_replace(array('+',false),array('.',false),$base64_string);
$salt = Aspis_substr($modified_base64_string,array(0,false),$length);
return $salt;
 }
function password_check ( $password,$existing_hash ) {
$hash = Aspis_crypt($password,$existing_hash);
if ( ($hash[0] === $existing_hash[0]))
 {return array(true,false);
}else 
{{return array(false,false);
}} }
function attempt_login ( $username,$password ) {
$admin = find_admin_by_username($username);
if ( $admin[0])
 {if ( deAspis(password_check($password,$admin[0]["hashed_password"])))
 {return $admin;
}else 
{{return array(false,false);
}}}else 
{{return array(false,false);
}} }
function logged_in (  ) {
return array((isset($_SESSION[0][('admin_id')]) && Aspis_isset( $_SESSION [0][('admin_id')])),false);
 }
function confirm_logged_in (  ) {
if ( (denot_boolean(logged_in())))
 {redirect_to(array("login.php",false));
} }
;
?>

<?php 
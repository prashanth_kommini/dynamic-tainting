<?php require_once('AspisMain.php'); ?><?php
function hello_dolly_get_lyric (  ) {
$lyrics = array("Hello, Dolly
Well, hello, Dolly
It's so nice to have you back where you belong
You're lookin' swell, Dolly
I can tell, Dolly
You're still glowin', you're still crowin'
You're still goin' strong
We feel the room swayin'
While the band's playin'
One of your old favourite songs from way back when
So, take her wrap, fellas
Find her an empty lap, fellas
Dolly'll never go away again
Hello, Dolly
Well, hello, Dolly
It's so nice to have you back where you belong
You're lookin' swell, Dolly
I can tell, Dolly
You're still glowin', you're still crowin'
You're still goin' strong
We feel the room swayin'
While the band's playin'
One of your old favourite songs from way back when
Golly, gee, fellas
Find her a vacant knee, fellas
Dolly'll never go away
Dolly'll never go away
Dolly'll never go away again",false);
$lyrics = Aspis_explode(array("\n",false),$lyrics);
return wptexturize(attachAspis($lyrics,mt_rand((0),(count($lyrics[0]) - (1)))));
 }
function hello_dolly (  ) {
$chosen = hello_dolly_get_lyric();
echo deAspis(AspisPrintGuard(concat2(concat1("<p id='dolly'>",$chosen),"</p>")));
 }
add_action(array('admin_footer',false),array('hello_dolly',false));
function dolly_css (  ) {
$x = (('rtl') == deAspis(get_bloginfo(array('text_direction',false)))) ? array('left',false) : array('right',false);
echo deAspis(AspisPrintGuard(concat2(concat1("
	<style type='text/css'>
	#dolly {
		position: absolute;
		top: 4.5em;
		margin: 0;
		padding: 0;
		",$x),": 215px;
		font-size: 11px;
	}
	</style>
	")));
 }
add_action(array('admin_head',false),array('dolly_css',false));
;
?>
<?php 
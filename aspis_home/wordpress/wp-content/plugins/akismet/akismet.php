<?php require_once('AspisMain.php'); ?><?php
define(('AKISMET_VERSION'),'2.2.7');
if ( defined(('WPCOM_API_KEY')))
 $wpcom_api_key = attAspisRC(constant(('WPCOM_API_KEY')));
else 
{$wpcom_api_key = array('',false);
}function akismet_init (  ) {
global $wpcom_api_key,$akismet_api_host,$akismet_api_port;
if ( $wpcom_api_key[0])
 $akismet_api_host = concat2($wpcom_api_key,'.rest.akismet.com');
else 
{$akismet_api_host = concat2(get_option(array('wordpress_api_key',false)),'.rest.akismet.com');
}$akismet_api_port = array(80,false);
add_action(array('admin_menu',false),array('akismet_config_page',false));
add_action(array('admin_menu',false),array('akismet_stats_page',false));
akismet_admin_warnings();
 }
add_action(array('init',false),array('akismet_init',false));
function akismet_admin_init (  ) {
if ( function_exists(('get_plugin_page_hook')))
 $hook = get_plugin_page_hook(array('akismet-stats-display',false),array('index.php',false));
else 
{$hook = array('dashboard_page_akismet-stats-display',false);
}add_action(concat1('admin_head-',$hook),array('akismet_stats_script',false));
 }
add_action(array('admin_init',false),array('akismet_admin_init',false));
if ( (!(function_exists(('wp_nonce_field')))))
 {function akismet_nonce_field ( $action = array(-1,false) ) {
return ;
 }
$akismet_nonce = negate(array(1,false));
}else 
{{function akismet_nonce_field ( $action = array(-1,false) ) {
return wp_nonce_field($action);
 }
$akismet_nonce = array('akismet-update-key',false);
}}if ( (!(function_exists(('number_format_i18n')))))
 {function number_format_i18n ( $number,$decimals = array(null,false) ) {
return attAspis(number_format($number[0],$decimals[0]));
 }
}function akismet_config_page (  ) {
if ( function_exists(('add_submenu_page')))
 add_submenu_page(array('plugins.php',false),__(array('Akismet Configuration',false)),__(array('Akismet Configuration',false)),array('manage_options',false),array('akismet-key-config',false),array('akismet_conf',false));
 }
function akismet_conf (  ) {
global $akismet_nonce,$wpcom_api_key;
if ( ((isset($_POST[0][('submit')]) && Aspis_isset( $_POST [0][('submit')]))))
 {if ( (function_exists(('current_user_can')) && (denot_boolean(current_user_can(array('manage_options',false))))))
 exit(deAspis(AspisPrintGuard(__(array('Cheatin&#8217; uh?',false)))));
check_admin_referer($akismet_nonce);
$key = Aspis_preg_replace(array('/[^a-h0-9]/i',false),array('',false),$_POST[0]['key']);
if ( ((empty($key) || Aspis_empty( $key))))
 {$key_status = array('empty',false);
arrayAssignAdd($ms[0][],addTaint(array('new_key_empty',false)));
delete_option(array('wordpress_api_key',false));
}else 
{{$key_status = akismet_verify_key($key);
}}if ( ($key_status[0] == ('valid')))
 {update_option(array('wordpress_api_key',false),$key);
arrayAssignAdd($ms[0][],addTaint(array('new_key_valid',false)));
}else 
{if ( ($key_status[0] == ('invalid')))
 {arrayAssignAdd($ms[0][],addTaint(array('new_key_invalid',false)));
}else 
{if ( ($key_status[0] == ('failed')))
 {arrayAssignAdd($ms[0][],addTaint(array('new_key_failed',false)));
}}}if ( ((isset($_POST[0][('akismet_discard_month')]) && Aspis_isset( $_POST [0][('akismet_discard_month')]))))
 update_option(array('akismet_discard_month',false),array('true',false));
else 
{update_option(array('akismet_discard_month',false),array('false',false));
}}elseif ( ((isset($_POST[0][('check')]) && Aspis_isset( $_POST [0][('check')]))))
 {akismet_get_server_connectivity(array(0,false));
}if ( ($key_status[0] != ('valid')))
 {$key = get_option(array('wordpress_api_key',false));
if ( ((empty($key) || Aspis_empty( $key))))
 {if ( ($key_status[0] != ('failed')))
 {if ( (deAspis(akismet_verify_key(array('1234567890ab',false))) == ('failed')))
 arrayAssignAdd($ms[0][],addTaint(array('no_connection',false)));
else 
{arrayAssignAdd($ms[0][],addTaint(array('key_empty',false)));
}}$key_status = array('empty',false);
}else 
{{$key_status = akismet_verify_key($key);
}}if ( ($key_status[0] == ('valid')))
 {arrayAssignAdd($ms[0][],addTaint(array('key_valid',false)));
}else 
{if ( ($key_status[0] == ('invalid')))
 {delete_option(array('wordpress_api_key',false));
arrayAssignAdd($ms[0][],addTaint(array('key_empty',false)));
}else 
{if ( ((!((empty($key) || Aspis_empty( $key)))) && ($key_status[0] == ('failed'))))
 {arrayAssignAdd($ms[0][],addTaint(array('key_failed',false)));
}}}}$messages = array(array('new_key_empty' => array(array('color' => array('aa0',false,false),deregisterTaint(array('text',false)) => addTaint(__(array('Your key has been cleared.',false)))),false,false),'new_key_valid' => array(array('color' => array('2d2',false,false),deregisterTaint(array('text',false)) => addTaint(__(array('Your key has been verified. Happy blogging!',false)))),false,false),'new_key_invalid' => array(array('color' => array('d22',false,false),deregisterTaint(array('text',false)) => addTaint(__(array('The key you entered is invalid. Please double-check it.',false)))),false,false),'new_key_failed' => array(array('color' => array('d22',false,false),deregisterTaint(array('text',false)) => addTaint(__(array('The key you entered could not be verified because a connection to akismet.com could not be established. Please check your server configuration.',false)))),false,false),'no_connection' => array(array('color' => array('d22',false,false),deregisterTaint(array('text',false)) => addTaint(__(array('There was a problem connecting to the Akismet server. Please check your server configuration.',false)))),false,false),'key_empty' => array(array('color' => array('aa0',false,false),deregisterTaint(array('text',false)) => addTaint(Aspis_sprintf(__(array('Please enter an API key. (<a href="%s" style="color:#fff">Get your key.</a>)',false)),array('http://akismet.com/get/',false)))),false,false),'key_valid' => array(array('color' => array('2d2',false,false),deregisterTaint(array('text',false)) => addTaint(__(array('This key is valid.',false)))),false,false),'key_failed' => array(array('color' => array('aa0',false,false),deregisterTaint(array('text',false)) => addTaint(__(array('The key below was previously validated but a connection to akismet.com can not be established at this time. Please check your server configuration.',false)))),false,false)),false);
;
?>
<?php if ( (!((empty($_POST[0][('submit')]) || Aspis_empty( $_POST [0][('submit')])))))
 {;
?>
<div id="message" class="updated fade"><p><strong><?php _e(array('Options saved.',false));
?></strong></p></div>
<?php };
?>
<div class="wrap">
<h2><?php _e(array('Akismet Configuration',false));
;
?></h2>
<div class="narrow">
<form action="" method="post" id="akismet-conf" style="margin: auto; width: 400px; ">
<?php if ( (denot_boolean($wpcom_api_key)))
 {;
?>
	<p><?php printf(deAspis(__(array('For many people, <a href="%1$s">Akismet</a> will greatly reduce or even completely eliminate the comment and trackback spam you get on your site. If one does happen to get through, simply mark it as "spam" on the moderation screen and Akismet will learn from the mistakes. If you don\'t have a WordPress.com account yet, you can get one at <a href="%2$s">Akismet.com</a>.',false))),'http://akismet.com/','http://akismet.com/get/');
;
?></p>

<?php akismet_nonce_field($akismet_nonce);
?>
<h3><label for="key"><?php _e(array('WordPress.com API Key',false));
;
?></label></h3>
<?php foreach ( $ms[0] as $m  )
{;
?>
	<p style="padding: .5em; background-color: #<?php echo deAspis(AspisPrintGuard($messages[0][$m[0]][0]['color']));
;
?>; color: #fff; font-weight: bold;"><?php echo deAspis(AspisPrintGuard($messages[0][$m[0]][0]['text']));
;
?></p>
<?php };
?>
<p><input id="key" name="key" type="text" size="15" maxlength="12" value="<?php echo deAspis(AspisPrintGuard(get_option(array('wordpress_api_key',false))));
;
?>" style="font-family: 'Courier New', Courier, mono; font-size: 1.5em;" /> (<?php _e(array('<a href="http://faq.wordpress.com/2005/10/19/api-key/">What is this?</a>',false));
;
?>)</p>
<?php if ( $invalid_key[0])
 {;
?>
<h3><?php _e(array('Why might my key be invalid?',false));
;
?></h3>
<p><?php _e(array('This can mean one of two things, either you copied the key wrong or that the plugin is unable to reach the Akismet servers, which is most often caused by an issue with your web host around firewalls or similar.',false));
;
?></p>
<?php };
?>
<?php };
?>
<p><label><input name="akismet_discard_month" id="akismet_discard_month" value="true" type="checkbox" <?php if ( (deAspis(get_option(array('akismet_discard_month',false))) == ('true')))
 echo deAspis(AspisPrintGuard(array(' checked="checked" ',false)));
;
?> /> <?php _e(array('Automatically discard spam comments on posts older than a month.',false));
;
?></label></p>
	<p class="submit"><input type="submit" name="submit" value="<?php _e(array('Update options &raquo;',false));
;
?>" /></p>
</form>

<form action="" method="post" id="akismet-connectivity" style="margin: auto; width: 400px; ">

<h3><?php _e(array('Server Connectivity',false));
;
?></h3>
<?php $servers = akismet_get_server_connectivity();
$fail_count = array(count($servers[0]) - count(deAspis(attAspisRC(array_filter(deAspisRC($servers))))),false);
if ( (is_array($servers[0]) && (count($servers[0]) > (0))))
 {if ( (($fail_count[0] > (0)) && ($fail_count[0] < count($servers[0]))))
 {;
?>
			<p style="padding: .5em; background-color: #aa0; color: #fff; font-weight:bold;"><?php _e(array('Unable to reach some Akismet servers.',false));
;
?></p>
			<p><?php echo deAspis(AspisPrintGuard(Aspis_sprintf(__(array('A network problem or firewall is blocking some connections from your web server to Akismet.com.  Akismet is working but this may cause problems during times of network congestion.  Please contact your web host or firewall administrator and give them <a href="%s" target="_blank">this information about Akismet and firewalls</a>.',false)),array('http://blog.akismet.com/akismet-hosting-faq/',false))));
;
?></p>
		<?php }elseif ( ($fail_count[0] > (0)))
 {;
?>
			<p style="padding: .5em; background-color: #d22; color: #fff; font-weight:bold;"><?php _e(array('Unable to reach any Akismet servers.',false));
;
?></p>
			<p><?php echo deAspis(AspisPrintGuard(Aspis_sprintf(__(array('A network problem or firewall is blocking all connections from your web server to Akismet.com.  <strong>Akismet cannot work correctly until this is fixed.</strong>  Please contact your web host or firewall administrator and give them <a href="%s" target="_blank">this information about Akismet and firewalls</a>.',false)),array('http://blog.akismet.com/akismet-hosting-faq/',false))));
;
?></p>
		<?php }else 
{{;
?>
			<p style="padding: .5em; background-color: #2d2; color: #fff; font-weight:bold;"><?php _e(array('All Akismet servers are available.',false));
;
?></p>
			<p><?php _e(array('Akismet is working correctly.  All servers are accessible.',false));
;
?></p>
		<?php }}}elseif ( (!(is_callable('fsockopen'))))
 {;
?>
			<p style="padding: .5em; background-color: #d22; color: #fff; font-weight:bold;"><?php _e(array('Network functions are disabled.',false));
;
?></p>
			<p><?php echo deAspis(AspisPrintGuard(Aspis_sprintf(__(array('Your web host or server administrator has disabled PHP\'s <code>fsockopen</code> function.  <strong>Akismet cannot work correctly until this is fixed.</strong>  Please contact your web host or firewall administrator and give them <a href="%s" target="_blank">this information about Akismet\'s system requirements</a>.',false)),array('http://blog.akismet.com/akismet-hosting-faq/',false))));
;
?></p>
		<?php }else 
{{;
?>
			<p style="padding: .5em; background-color: #d22; color: #fff; font-weight:bold;"><?php _e(array('Unable to find Akismet servers.',false));
;
?></p>
			<p><?php echo deAspis(AspisPrintGuard(Aspis_sprintf(__(array('A DNS problem or firewall is preventing all access from your web server to Akismet.com.  <strong>Akismet cannot work correctly until this is fixed.</strong>  Please contact your web host or firewall administrator and give them <a href="%s" target="_blank">this information about Akismet and firewalls</a>.',false)),array('http://blog.akismet.com/akismet-hosting-faq/',false))));
;
?></p>
		<?php }}if ( (!((empty($servers) || Aspis_empty( $servers)))))
 {;
?>
<table style="width: 100%;">
<thead><th><?php _e(array('Akismet server',false));
;
?></th><th><?php _e(array('Network Status',false));
;
?></th></thead>
<tbody>
<?php AspisInternalFunctionCall("asort",AspisPushRefParam($servers),array(0));
foreach ( $servers[0] as $ip =>$status )
{restoreTaint($ip,$status);
{$color = ($status[0] ? array('#2d2',false) : array('#d22',false));
;
?>
		<tr>
		<td><?php echo deAspis(AspisPrintGuard(AspisKillTaint(Aspis_htmlspecialchars($ip),0)));
;
?></td>
		<td style="padding: 0 .5em; font-weight:bold; color: #fff; background-color: <?php echo deAspis(AspisPrintGuard($color));
;
?>"><?php echo deAspis(AspisPrintGuard(($status[0] ? __(array('No problems',false)) : __(array('Obstructed',false)))));
;
?></td>
		
	<?php }}};
?>
</tbody>
</table>
	<p><?php if ( deAspis(get_option(array('akismet_connectivity_time',false))))
 echo deAspis(AspisPrintGuard(Aspis_sprintf(__(array('Last checked %s ago.',false)),human_time_diff(get_option(array('akismet_connectivity_time',false))))));
;
?></p>
	<p class="submit"><input type="submit" name="check" value="<?php _e(array('Check network status &raquo;',false));
;
?>" /></p>
</form>

</div>
</div>
<?php  }
function akismet_stats_page (  ) {
if ( function_exists(('add_submenu_page')))
 add_submenu_page(array('index.php',false),__(array('Akismet Stats',false)),__(array('Akismet Stats',false)),array('manage_options',false),array('akismet-stats-display',false),array('akismet_stats_display',false));
 }
function akismet_stats_script (  ) {
;
?>
<script type="text/javascript">
function resizeIframe() {
    var height = document.documentElement.clientHeight;
    height -= document.getElementById('akismet-stats-frame').offsetTop;
    height += 100; // magic padding
    
    document.getElementById('akismet-stats-frame').style.height = height +"px";
    
};
function resizeIframeInit() {
	document.getElementById('akismet-stats-frame').onload = resizeIframe;
	window.onresize = resizeIframe;
}
addLoadEvent(resizeIframeInit);
</script><?php  }
function akismet_stats_display (  ) {
global $akismet_api_host,$akismet_api_port,$wpcom_api_key;
$blog = Aspis_urlencode(get_option(array('home',false)));
$url = concat(concat1("http://",akismet_get_key()),concat1(".web.akismet.com/1.0/user-stats.php?blog=",$blog));
;
?>
	<div class="wrap">
	<iframe src="<?php echo deAspis(AspisPrintGuard($url));
;
?>" width="100%" height="100%" frameborder="0" id="akismet-stats-frame"></iframe>
	</div>
	<?php  }
function akismet_get_key (  ) {
global $wpcom_api_key;
if ( (!((empty($wpcom_api_key) || Aspis_empty( $wpcom_api_key)))))
 return $wpcom_api_key;
return get_option(array('wordpress_api_key',false));
 }
function akismet_verify_key ( $key,$ip = array(null,false) ) {
global $akismet_api_host,$akismet_api_port,$wpcom_api_key;
$blog = Aspis_urlencode(get_option(array('home',false)));
if ( $wpcom_api_key[0])
 $key = $wpcom_api_key;
$response = akismet_http_post(concat(concat2(concat1("key=",$key),"&blog="),$blog),array('rest.akismet.com',false),array('/1.1/verify-key',false),$akismet_api_port,$ip);
if ( (((!(is_array($response[0]))) || (!((isset($response[0][(1)]) && Aspis_isset( $response [0][(1)]))))) || ((deAspis(attachAspis($response,(1))) != ('valid')) && (deAspis(attachAspis($response,(1))) != ('invalid')))))
 return array('failed',false);
return attachAspis($response,(1));
 }
function akismet_check_server_connectivity (  ) {
global $akismet_api_host,$akismet_api_port,$wpcom_api_key;
$test_host = array('rest.akismet.com',false);
if ( ((!(is_callable('fsockopen'))) || (!(is_callable('gethostbynamel')))))
 return array(array(),false);
$ips = attAspisRC(gethostbynamel($test_host[0]));
if ( (((denot_boolean($ips)) || (!(is_array($ips[0])))) || (!(count($ips[0])))))
 return array(array(),false);
$servers = array(array(),false);
foreach ( $ips[0] as $ip  )
{$response = akismet_verify_key(akismet_get_key(),$ip);
if ( (($response[0] == ('valid')) || ($response[0] == ('invalid'))))
 arrayAssign($servers[0],deAspis(registerTaint($ip)),addTaint(array(true,false)));
else 
{arrayAssign($servers[0],deAspis(registerTaint($ip)),addTaint(array(false,false)));
}}return $servers;
 }
function akismet_get_server_connectivity ( $cache_timeout = array(86400,false) ) {
$servers = get_option(array('akismet_available_servers',false));
if ( (((time() - deAspis(get_option(array('akismet_connectivity_time',false)))) < $cache_timeout[0]) && ($servers[0] !== false)))
 return $servers;
$servers = akismet_check_server_connectivity();
update_option(array('akismet_available_servers',false),$servers);
update_option(array('akismet_connectivity_time',false),attAspis(time()));
return $servers;
 }
function akismet_server_connectivity_ok (  ) {
global $wpcom_api_key;
if ( $wpcom_api_key[0])
 return array(true,false);
$servers = akismet_get_server_connectivity();
return not_boolean((array((((empty($servers) || Aspis_empty( $servers))) || (!(count($servers[0])))) || (count(deAspis(attAspisRC(array_filter(deAspisRC($servers))))) < count($servers[0])),false)));
 }
function akismet_admin_warnings (  ) {
global $wpcom_api_key;
if ( (((denot_boolean(get_option(array('wordpress_api_key',false)))) && (denot_boolean($wpcom_api_key))) && (!((isset($_POST[0][('submit')]) && Aspis_isset( $_POST [0][('submit')]))))))
 {function akismet_warning (  ) {
echo deAspis(AspisPrintGuard(concat2(concat(concat2(concat1("
			<div id='akismet-warning' class='updated fade'><p><strong>",__(array('Akismet is almost ready.',false))),"</strong> "),Aspis_sprintf(__(array('You must <a href="%1$s">enter your WordPress.com API key</a> for it to work.',false)),array("plugins.php?page=akismet-key-config",false))),"</p></div>
			")));
 }
add_action(array('admin_notices',false),array('akismet_warning',false));
return ;
}elseif ( (((deAspis(get_option(array('akismet_connectivity_time',false))) && ((empty($_POST) || Aspis_empty( $_POST)))) && deAspis(is_admin())) && (denot_boolean(akismet_server_connectivity_ok()))))
 {function akismet_warning (  ) {
echo deAspis(AspisPrintGuard(concat2(concat(concat2(concat1("
			<div id='akismet-warning' class='updated fade'><p><strong>",__(array('Akismet has detected a problem.',false))),"</strong> "),Aspis_sprintf(__(array('A server or network problem is preventing Akismet from working correctly.  <a href="%1$s">Click here for more information</a> about how to fix the problem.',false)),array("plugins.php?page=akismet-key-config",false))),"</p></div>
			")));
 }
add_action(array('admin_notices',false),array('akismet_warning',false));
return ;
} }
function akismet_get_host ( $host ) {
if ( deAspis(akismet_server_connectivity_ok()))
 {return $host;
}else 
{{$ips = akismet_get_server_connectivity();
if ( ((count($ips[0]) > (0)) && (count(deAspis(attAspisRC(array_filter(deAspisRC($ips))))) < count($ips[0]))))
 {$dns = array_cast(attAspisRC(gethostbynamel((deconcat2(Aspis_rtrim($host,array('.',false)),'.')))));
$dns = attAspisRC(array_filter(deAspisRC($dns)));
foreach ( $dns[0] as $ip  )
{if ( (array_key_exists(deAspisRC($ip),deAspisRC($ips)) && ((empty($ips[0][$ip[0]]) || Aspis_empty( $ips [0][$ip[0]])))))
 unset($dns[0][$ip[0]]);
}if ( count($dns[0]))
 return attachAspis($dns,deAspis(Aspis_array_rand($dns)));
}}}return $host;
 }
function akismet_http_post ( $request,$host,$path,$port = array(80,false),$ip = array(null,false) ) {
global $wp_version;
$akismet_version = attAspisRC(constant(('AKISMET_VERSION')));
$http_request = concat2(concat1("POST ",$path)," HTTP/1.0\r\n");
$http_request = concat($http_request,concat2(concat1("Host: ",$host),"\r\n"));
$http_request = concat($http_request,concat2(concat1("Content-Type: application/x-www-form-urlencoded; charset=",get_option(array('blog_charset',false))),"\r\n"));
$http_request = concat($http_request,concat2(concat1("Content-Length: ",attAspis(strlen($request[0]))),"\r\n"));
$http_request = concat($http_request,concat2(concat(concat2(concat1("User-Agent: WordPress/",$wp_version)," | Akismet/"),$akismet_version),"\r\n"));
$http_request = concat2($http_request,"\r\n");
$http_request = concat($http_request,$request);
$http_host = $host;
if ( ($ip[0] && long2ip(ip2long($ip[0]))))
 {$http_host = $ip;
}else 
{{$http_host = akismet_get_host($host);
}}$response = array('',false);
if ( (false != deAspis(($fs = @AspisInternalFunctionCall("fsockopen",$http_host[0],$port[0],AspisPushRefParam($errno),AspisPushRefParam($errstr),(10),array(2,3))))))
 {fwrite($fs[0],$http_request[0]);
while ( (!(feof($fs[0]))) )
$response = concat($response,attAspis(fgets($fs[0],(1160))));
fclose($fs[0]);
$response = Aspis_explode(array("\r\n\r\n",false),$response,array(2,false));
}return $response;
 }
function akismet_result_spam ( $approved ) {
if ( deAspis($incr = apply_filters(array('akismet_spam_count_incr',false),array(1,false))))
 update_option(array('akismet_spam_count',false),array(deAspis(get_option(array('akismet_spam_count',false))) + $incr[0],false));
return array('spam',false);
 }
function akismet_auto_check_comment ( $comment ) {
global $akismet_api_host,$akismet_api_port;
arrayAssign($comment[0],deAspis(registerTaint(array('user_ip',false))),addTaint(Aspis_preg_replace(array('/[^0-9., ]/',false),array('',false),$_SERVER[0]['REMOTE_ADDR'])));
arrayAssign($comment[0],deAspis(registerTaint(array('user_agent',false))),addTaint($_SERVER[0]['HTTP_USER_AGENT']));
arrayAssign($comment[0],deAspis(registerTaint(array('referrer',false))),addTaint($_SERVER[0]['HTTP_REFERER']));
arrayAssign($comment[0],deAspis(registerTaint(array('blog',false))),addTaint(get_option(array('home',false))));
arrayAssign($comment[0],deAspis(registerTaint(array('blog_lang',false))),addTaint(get_locale()));
arrayAssign($comment[0],deAspis(registerTaint(array('blog_charset',false))),addTaint(get_option(array('blog_charset',false))));
arrayAssign($comment[0],deAspis(registerTaint(array('permalink',false))),addTaint(get_permalink($comment[0]['comment_post_ID'])));
$ignore = array(array(array('HTTP_COOKIE',false)),false);
foreach ( $_SERVER[0] as $key =>$value )
{restoreTaint($key,$value);
if ( ((denot_boolean(Aspis_in_array($key,$ignore))) && is_string(deAspisRC($value))))
 arrayAssign($comment[0],deAspis(registerTaint($key)),addTaint($value));
}$query_string = array('',false);
foreach ( $comment[0] as $key =>$data )
{restoreTaint($key,$data);
$query_string = concat($query_string,concat2(concat(concat2($key,'='),Aspis_urlencode(Aspis_stripslashes($data))),'&'));
}$response = akismet_http_post($query_string,$akismet_api_host,array('/1.1/comment-check',false),$akismet_api_port);
if ( (('true') == deAspis(attachAspis($response,(1)))))
 {add_filter(array('pre_comment_approved',false),array('akismet_result_spam',false));
do_action(array('akismet_spam_caught',false));
$post = get_post($comment[0]['comment_post_ID']);
$last_updated = attAspis(strtotime($post[0]->post_modified_gmt[0]));
$diff = array(time() - $last_updated[0],false);
$diff = array($diff[0] / (86400),false);
if ( ((($post[0]->post_type[0] == ('post')) && ($diff[0] > (30))) && (deAspis(get_option(array('akismet_discard_month',false))) == ('true'))))
 {if ( deAspis($incr = apply_filters(array('akismet_spam_count_incr',false),array(1,false))))
 update_option(array('akismet_spam_count',false),array(deAspis(get_option(array('akismet_spam_count',false))) + $incr[0],false));
exit();
}}akismet_delete_old();
return $comment;
 }
function akismet_delete_old (  ) {
global $wpdb;
$now_gmt = current_time(array('mysql',false),array(1,false));
$wpdb[0]->query(concat2(concat(concat2(concat1("DELETE FROM ",$wpdb[0]->comments)," WHERE DATE_SUB('"),$now_gmt),"', INTERVAL 15 DAY) > comment_date_gmt AND comment_approved = 'spam'"));
$n = attAspis(mt_rand((1),(5000)));
if ( ($n[0] == (11)))
 $wpdb[0]->query(concat1("OPTIMIZE TABLE ",$wpdb[0]->comments));
 }
function akismet_submit_nonspam_comment ( $comment_id ) {
global $wpdb,$akismet_api_host,$akismet_api_port,$current_user,$current_site;
$comment_id = int_cast($comment_id);
$comment = $wpdb[0]->get_row(concat2(concat(concat2(concat1("SELECT * FROM ",$wpdb[0]->comments)," WHERE comment_ID = '"),$comment_id),"'"));
if ( (denot_boolean($comment)))
 return ;
$comment[0]->blog = get_option(array('home',false));
$comment[0]->blog_lang = get_locale();
$comment[0]->blog_charset = get_option(array('blog_charset',false));
$comment[0]->permalink = get_permalink($comment[0]->comment_post_ID);
if ( is_object($current_user[0]))
 {$comment[0]->reporter = $current_user[0]->user_login;
}if ( is_object($current_site[0]))
 {$comment[0]->site_domain = $current_site[0]->domain;
}$query_string = array('',false);
foreach ( $comment[0] as $key =>$data )
{restoreTaint($key,$data);
$query_string = concat($query_string,concat2(concat(concat2($key,'='),Aspis_urlencode(Aspis_stripslashes($data))),'&'));
}$response = akismet_http_post($query_string,$akismet_api_host,array("/1.1/submit-ham",false),$akismet_api_port);
 }
function akismet_submit_spam_comment ( $comment_id ) {
global $wpdb,$akismet_api_host,$akismet_api_port,$current_user,$current_site;
$comment_id = int_cast($comment_id);
$comment = $wpdb[0]->get_row(concat2(concat(concat2(concat1("SELECT * FROM ",$wpdb[0]->comments)," WHERE comment_ID = '"),$comment_id),"'"));
if ( (denot_boolean($comment)))
 return ;
if ( (('spam') != $comment[0]->comment_approved[0]))
 return ;
$comment[0]->blog = get_option(array('home',false));
$comment[0]->blog_lang = get_locale();
$comment[0]->blog_charset = get_option(array('blog_charset',false));
$comment[0]->permalink = get_permalink($comment[0]->comment_post_ID);
if ( is_object($current_user[0]))
 {$comment[0]->reporter = $current_user[0]->user_login;
}if ( is_object($current_site[0]))
 {$comment[0]->site_domain = $current_site[0]->domain;
}$query_string = array('',false);
foreach ( $comment[0] as $key =>$data )
{restoreTaint($key,$data);
$query_string = concat($query_string,concat2(concat(concat2($key,'='),Aspis_urlencode(Aspis_stripslashes($data))),'&'));
}$response = akismet_http_post($query_string,$akismet_api_host,array("/1.1/submit-spam",false),$akismet_api_port);
 }
add_action(array('wp_set_comment_status',false),array('akismet_submit_spam_comment',false));
add_action(array('edit_comment',false),array('akismet_submit_spam_comment',false));
add_action(array('preprocess_comment',false),array('akismet_auto_check_comment',false),array(1,false));
function akismet_spamtoham ( $comment ) {
akismet_submit_nonspam_comment($comment[0]->comment_ID);
 }
add_filter(array('comment_spam_to_approved',false),array('akismet_spamtoham',false));
function akismet_spam_count ( $type = array(false,false) ) {
global $wpdb;
if ( (denot_boolean($type)))
 {$count = wp_cache_get(array('akismet_spam_count',false),array('widget',false));
if ( (false === $count[0]))
 {if ( function_exists(('wp_count_comments')))
 {$count = wp_count_comments();
$count = $count[0]->spam;
}else 
{{$count = int_cast($wpdb[0]->get_var(concat2(concat1("SELECT COUNT(comment_ID) FROM ",$wpdb[0]->comments)," WHERE comment_approved = 'spam'")));
}}wp_cache_set(array('akismet_spam_count',false),$count,array('widget',false),array(3600,false));
}return $count;
}elseif ( ((('comments') == $type[0]) || (('comment') == $type[0])))
 {$type = array('',false);
}else 
{{$type = $wpdb[0]->escape($type);
}}return int_cast($wpdb[0]->get_var(concat2(concat(concat2(concat1("SELECT COUNT(comment_ID) FROM ",$wpdb[0]->comments)," WHERE comment_approved = 'spam' AND comment_type='"),$type),"'")));
 }
function akismet_spam_comments ( $type = array(false,false),$page = array(1,false),$per_page = array(50,false) ) {
global $wpdb;
$page = int_cast($page);
if ( ($page[0] < (2)))
 $page = array(1,false);
$per_page = int_cast($per_page);
if ( ($per_page[0] < (1)))
 $per_page = array(50,false);
$start = array(($page[0] - (1)) * $per_page[0],false);
$end = array($start[0] + $per_page[0],false);
if ( $type[0])
 {if ( ((('comments') == $type[0]) || (('comment') == $type[0])))
 $type = array('',false);
else 
{$type = $wpdb[0]->escape($type);
}return $wpdb[0]->get_results(concat(concat2(concat(concat2(concat(concat2(concat1("SELECT * FROM ",$wpdb[0]->comments)," WHERE comment_approved = 'spam' AND comment_type='"),$type),"' ORDER BY comment_date DESC LIMIT "),$start),", "),$end));
}return $wpdb[0]->get_results(concat(concat2(concat(concat2(concat1("SELECT * FROM ",$wpdb[0]->comments)," WHERE comment_approved = 'spam' ORDER BY comment_date DESC LIMIT "),$start),", "),$end));
 }
function akismet_spam_totals (  ) {
global $wpdb;
$totals = $wpdb[0]->get_results(concat2(concat1("SELECT comment_type, COUNT(*) AS cc FROM ",$wpdb[0]->comments)," WHERE comment_approved = 'spam' GROUP BY comment_type"));
$return = array(array(),false);
foreach ( $totals[0] as $total  )
arrayAssign($return[0],deAspis(registerTaint($total[0]->comment_type[0] ? $total[0]->comment_type : array('comment',false))),addTaint($total[0]->cc));
return $return;
 }
function akismet_manage_page (  ) {
global $wpdb,$submenu,$wp_db_version;
if ( ((8645) <= $wp_db_version[0]))
 return ;
$count = Aspis_sprintf(__(array('Akismet Spam (%s)',false)),akismet_spam_count());
if ( ((isset($submenu[0][('edit-comments.php')]) && Aspis_isset( $submenu [0][('edit-comments.php')]))))
 add_submenu_page(array('edit-comments.php',false),__(array('Akismet Spam',false)),$count,array('moderate_comments',false),array('akismet-admin',false),array('akismet_caught',false));
elseif ( function_exists(('add_management_page')))
 add_management_page(__(array('Akismet Spam',false)),$count,array('moderate_comments',false),array('akismet-admin',false),array('akismet_caught',false));
 }
function akismet_caught (  ) {
global $wpdb,$comment,$akismet_caught,$akismet_nonce;
akismet_recheck_queue();
if ( ((((isset($_POST[0][('submit')]) && Aspis_isset( $_POST [0][('submit')]))) && (('recover') == deAspis($_POST[0]['action']))) && (!((empty($_POST[0][('not_spam')]) || Aspis_empty( $_POST [0][('not_spam')]))))))
 {check_admin_referer($akismet_nonce);
if ( (function_exists(('current_user_can')) && (denot_boolean(current_user_can(array('moderate_comments',false))))))
 exit(deAspis(AspisPrintGuard(__(array('You do not have sufficient permission to moderate comments.',false)))));
$i = array(0,false);
foreach ( deAspis($_POST[0]['not_spam']) as $comment  )
{$comment = int_cast($comment);
if ( function_exists(('wp_set_comment_status')))
 wp_set_comment_status($comment,array('approve',false));
else 
{$wpdb[0]->query(concat2(concat(concat2(concat1("UPDATE ",$wpdb[0]->comments)," SET comment_approved = '1' WHERE comment_ID = '"),$comment),"'"));
}akismet_submit_nonspam_comment($comment);
preincr($i);
}$to = add_query_arg(array('recovered',false),$i,$_SERVER[0]['HTTP_REFERER']);
wp_redirect($to);
exit();
}if ( (('delete') == deAspis($_POST[0]['action'])))
 {check_admin_referer($akismet_nonce);
if ( (function_exists(('current_user_can')) && (denot_boolean(current_user_can(array('moderate_comments',false))))))
 exit(deAspis(AspisPrintGuard(__(array('You do not have sufficient permission to moderate comments.',false)))));
$delete_time = $wpdb[0]->escape($_POST[0]['display_time']);
$nuked = $wpdb[0]->query(concat2(concat(concat2(concat1("DELETE FROM ",$wpdb[0]->comments)," WHERE comment_approved = 'spam' AND '"),$delete_time),"' > comment_date_gmt"));
wp_cache_delete(array('akismet_spam_count',false),array('widget',false));
$to = add_query_arg(array('deleted',false),array('all',false),$_SERVER[0]['HTTP_REFERER']);
wp_redirect($to);
exit();
}if ( ((isset($_GET[0][('recovered')]) && Aspis_isset( $_GET [0][('recovered')]))))
 {$i = int_cast($_GET[0]['recovered']);
echo deAspis(AspisPrintGuard(concat2(concat1('<div class="updated"><p>',Aspis_sprintf(__(array('%1$s comments recovered.',false)),$i)),"</p></div>")));
}if ( ((isset($_GET[0][('deleted')]) && Aspis_isset( $_GET [0][('deleted')]))))
 echo deAspis(AspisPrintGuard(concat2(concat1('<div class="updated"><p>',__(array('All spam deleted.',false))),'</p></div>')));
if ( ((isset($GLOBALS[0][('submenu')][0][('edit-comments.php')]) && Aspis_isset( $GLOBALS [0][('submenu')] [0][('edit-comments.php')]))))
 $link = array('edit-comments.php',false);
else 
{$link = array('edit.php',false);
};
?>
<style type="text/css">
.akismet-tabs {
	list-style: none;
	margin: 0;
	padding: 0;
	clear: both;
	border-bottom: 1px solid #ccc;
	height: 31px;
	margin-bottom: 20px;
	background: #ddd;
	border-top: 1px solid #bdbdbd;
}
.akismet-tabs li {
	float: left;
	margin: 5px 0 0 20px;
}
.akismet-tabs a {
	display: block;
	padding: 4px .5em 3px;
	border-bottom: none;
	color: #036;
}
.akismet-tabs .active a {
	background: #fff;
	border: 1px solid #ccc;
	border-bottom: none;
	color: #000;
	font-weight: bold;
	padding-bottom: 4px;
}
#akismetsearch {
	float: right;
	margin-top: -.5em;
}

#akismetsearch p {
	margin: 0;
	padding: 0;
}
</style>
<div class="wrap">
<h2><?php _e(array('Caught Spam',false));
?></h2>
<?php $count = get_option(array('akismet_spam_count',false));
if ( $count[0])
 {;
?>
<p><?php printf(deAspis(__(array('Akismet has caught <strong>%1$s spam</strong> for you since you first installed it.',false))),deAspisRC(number_format_i18n($count)));
;
?></p>
<?php }$spam_count = akismet_spam_count();
if ( ((0) == $spam_count[0]))
 {echo deAspis(AspisPrintGuard(concat2(concat1('<p>',__(array('You have no spam currently in the queue. Must be your lucky day. :)',false))),'</p>')));
echo deAspis(AspisPrintGuard(array('</div>',false)));
}else 
{{echo deAspis(AspisPrintGuard(concat2(concat1('<p>',__(array('You can delete all of the spam from your database with a single click. This operation cannot be undone, so you may wish to check to ensure that no legitimate comments got through first. Spam is automatically deleted after 15 days, so don&#8217;t sweat it.',false))),'</p>')));
;
?>
<?php if ( (!((isset($_POST[0][('s')]) && Aspis_isset( $_POST [0][('s')])))))
 {;
?>
<form method="post" action="<?php echo deAspis(AspisPrintGuard(attribute_escape(add_query_arg(array('noheader',false),array('true',false)))));
;
?>">
<?php akismet_nonce_field($akismet_nonce);
?>
<input type="hidden" name="action" value="delete" />
<?php printf(deAspis(__(array('There are currently %1$s comments identified as spam.',false))),deAspisRC($spam_count));
;
?>&nbsp; &nbsp; <input type="submit" class="button delete" name="Submit" value="<?php _e(array('Delete all',false));
;
?>" />
<input type="hidden" name="display_time" value="<?php echo deAspis(AspisPrintGuard(current_time(array('mysql',false),array(1,false))));
;
?>" />
</form>
<?php };
?>
</div>
<div class="wrap">
<?php if ( ((isset($_POST[0][('s')]) && Aspis_isset( $_POST [0][('s')]))))
 {;
?>
<h2><?php _e(array('Search',false));
;
?></h2>
<?php }else 
{{;
?>
<?php echo deAspis(AspisPrintGuard(concat2(concat1('<p>',__(array('These are the latest comments identified as spam by Akismet. If you see any mistakes, simply mark the comment as "not spam" and Akismet will learn from the submission. If you wish to recover a comment from spam, simply select the comment, and click Not Spam. After 15 days we clean out the junk for you.',false))),'</p>')));
;
?>
<?php }};
?>
<?php if ( ((isset($_POST[0][('s')]) && Aspis_isset( $_POST [0][('s')]))))
 {$s = $wpdb[0]->escape($_POST[0]['s']);
$comments = $wpdb[0]->get_results(concat2(concat(concat2(concat(concat2(concat(concat2(concat(concat2(concat(concat2(concat1("SELECT * FROM ",$wpdb[0]->comments),"  WHERE
		(comment_author LIKE '%"),$s),"%' OR
		comment_author_email LIKE '%"),$s),"%' OR
		comment_author_url LIKE ('%"),$s),"%') OR
		comment_author_IP LIKE ('%"),$s),"%') OR
		comment_content LIKE ('%"),$s),"%') ) AND
		comment_approved = 'spam'
		ORDER BY comment_date DESC"));
}else 
{{if ( ((isset($_GET[0][('apage')]) && Aspis_isset( $_GET [0][('apage')]))))
 $page = int_cast($_GET[0]['apage']);
else 
{$page = array(1,false);
}if ( ($page[0] < (2)))
 $page = array(1,false);
$current_type = array(false,false);
if ( ((isset($_GET[0][('ctype')]) && Aspis_isset( $_GET [0][('ctype')]))))
 $current_type = Aspis_preg_replace(array('|[^a-z]|',false),array('',false),$_GET[0]['ctype']);
$comments = akismet_spam_comments($current_type,$page);
$total = akismet_spam_count($current_type);
$totals = akismet_spam_totals();
;
?>
<ul class="akismet-tabs">
<li <?php if ( (!((isset($_GET[0][('ctype')]) && Aspis_isset( $_GET [0][('ctype')])))))
 echo deAspis(AspisPrintGuard(array(' class="active"',false)));
;
?>><a href="edit-comments.php?page=akismet-admin"><?php _e(array('All',false));
;
?></a></li>
<?php foreach ( $totals[0] as $type =>$type_count )
{restoreTaint($type,$type_count);
{if ( (('comment') == $type[0]))
 {$type = array('comments',false);
$show = __(array('Comments',false));
}else 
{{$show = Aspis_ucwords($type);
}}$type_count = number_format_i18n($type_count);
$extra = ($current_type[0] === $type[0]) ? array(' class="active"',false) : array('',false);
echo deAspis(AspisPrintGuard(concat2(concat(concat2(concat(concat2(concat(concat2(concat1("<li ",$extra),"><a href='edit-comments.php?page=akismet-admin&amp;ctype="),$type),"'>"),$show)," ("),$type_count),")</a></li>")));
}}do_action(array('akismet_tabs',false));
;
?>
</ul>
<?php }}if ( $comments[0])
 {;
?>
<form method="post" action="<?php echo deAspis(AspisPrintGuard(attribute_escape(concat2($link,"?page=akismet-admin"))));
;
?>" id="akismetsearch">
<p>  <input type="text" name="s" value="<?php if ( ((isset($_POST[0][('s')]) && Aspis_isset( $_POST [0][('s')]))))
 echo deAspis(AspisPrintGuard(attribute_escape($_POST[0]['s'])));
;
?>" size="17" />
  <input type="submit" class="button" name="submit" value="<?php echo deAspis(AspisPrintGuard(attribute_escape(__(array('Search Spam &raquo;',false)))));
?>"  />  </p>
</form>
<?php if ( ($total[0] > (50)))
 {$total_pages = attAspis(ceil(($total[0] / (50))));
$r = array('',false);
if ( ((1) < $page[0]))
 {arrayAssign($args[0],deAspis(registerTaint(array('apage',false))),addTaint(((1) == ($page[0] - (1))) ? array('',false) : array($page[0] - (1),false)));
$r = concat($r,concat2(concat2(concat(concat2(concat1('<a class="prev" href="',clean_url(add_query_arg($args))),'">'),__(array('&laquo; Previous Page',false))),'</a>'),"\n"));
}if ( (deAspis(($total_pages = attAspis(ceil(($total[0] / (50)))))) > (1)))
 {for ( $page_num = array(1,false) ; ($page_num[0] <= $total_pages[0]) ; postincr($page_num) )
{if ( ($page[0] == $page_num[0]))
 {$r = concat($r,concat2(concat1("<strong>",$page_num),"</strong>\n"));
}else 
{$p = array(false,false);
if ( ((($page_num[0] < (3)) || (($page_num[0] >= ($page[0] - (3))) && ($page_num[0] <= ($page[0] + (3))))) || ($page_num[0] > ($total_pages[0] - (3)))))
 {arrayAssign($args[0],deAspis(registerTaint(array('apage',false))),addTaint(((1) == $page_num[0]) ? array('',false) : $page_num));
$r = concat($r,concat2(concat(concat2(concat1('<a class="page-numbers" href="',clean_url(add_query_arg($args))),'">'),($page_num)),"</a>\n"));
$in = array(true,false);
}elseif ( ($in[0] == true))
 {$r = concat2($r,"...\n");
$in = array(false,false);
}}}}if ( (((deAspis(($page)) * (50)) < $total[0]) || (deAspis(negate(array(1,false))) == $total[0])))
 {arrayAssign($args[0],deAspis(registerTaint(array('apage',false))),addTaint(array($page[0] + (1),false)));
$r = concat($r,concat2(concat2(concat(concat2(concat1('<a class="next" href="',clean_url(add_query_arg($args))),'">'),__(array('Next Page &raquo;',false))),'</a>'),"\n"));
}echo deAspis(AspisPrintGuard(concat2(concat1("<p>",$r),"</p>")));
;
?>

<?php };
?>
<form style="clear: both;" method="post" action="<?php echo deAspis(AspisPrintGuard(attribute_escape(add_query_arg(array('noheader',false),array('true',false)))));
;
?>">
<?php akismet_nonce_field($akismet_nonce);
?>
<input type="hidden" name="action" value="recover" />
<ul id="spam-list" class="commentlist" style="list-style: none; margin: 0; padding: 0;">
<?php $i = array(0,false);
foreach ( $comments[0] as $comment  )
{postincr($i);
$comment_date = mysql2date(concat(concat2(get_option(array("date_format",false))," @ "),get_option(array("time_format",false))),$comment[0]->comment_date);
$post = get_post($comment[0]->comment_post_ID);
$post_title = $post[0]->post_title;
if ( ($i[0] % (2)))
 $class = array('class="alternate"',false);
else 
{$class = array('',false);
}echo deAspis(AspisPrintGuard(concat2(concat(concat2(concat1("\n\t<li id='comment-",$comment[0]->comment_ID),"' "),$class),">")));
;
?>

<p><strong><?php comment_author();
?></strong> <?php if ( $comment[0]->comment_author_email[0])
 {;
?>| <?php comment_author_email_link();
?> <?php }if ( ($comment[0]->comment_author_url[0] && (('http://') != $comment[0]->comment_author_url[0])))
 {;
?> | <?php comment_author_url_link();
?> <?php };
?>| <?php _e(array('IP:',false));
?> <a href="http://ws.arin.net/cgi-bin/whois.pl?queryinput=<?php comment_author_IP();
?>"><?php comment_author_IP();
?></a></p>

<?php comment_text();
?>

<p><label for="spam-<?php echo deAspis(AspisPrintGuard($comment[0]->comment_ID));
;
?>">
<input type="checkbox" id="spam-<?php echo deAspis(AspisPrintGuard($comment[0]->comment_ID));
;
?>" name="not_spam[]" value="<?php echo deAspis(AspisPrintGuard($comment[0]->comment_ID));
;
?>" />
<?php _e(array('Not Spam',false));
?></label> &#8212; <?php comment_date(array('M j, g:i A',false));
;
?> &#8212; [
<?php $post = get_post($comment[0]->comment_post_ID);
$post_title = wp_specialchars($post[0]->post_title,array('double',false));
$post_title = (('') == $post_title[0]) ? concat1("# ",$comment[0]->comment_post_ID) : $post_title;
;
?>
 <a href="<?php echo deAspis(AspisPrintGuard(get_permalink($comment[0]->comment_post_ID)));
;
?>" title="<?php echo deAspis(AspisPrintGuard($post_title));
;
?>"><?php _e(array('View Post',false));
?></a> ] </p>


<?php };
?>
</ul>
<?php if ( ($total[0] > (50)))
 {$total_pages = attAspis(ceil(($total[0] / (50))));
$r = array('',false);
if ( ((1) < $page[0]))
 {arrayAssign($args[0],deAspis(registerTaint(array('apage',false))),addTaint(((1) == ($page[0] - (1))) ? array('',false) : array($page[0] - (1),false)));
$r = concat($r,concat2(concat2(concat(concat2(concat1('<a class="prev" href="',clean_url(add_query_arg($args))),'">'),__(array('&laquo; Previous Page',false))),'</a>'),"\n"));
}if ( (deAspis(($total_pages = attAspis(ceil(($total[0] / (50)))))) > (1)))
 {for ( $page_num = array(1,false) ; ($page_num[0] <= $total_pages[0]) ; postincr($page_num) )
{if ( ($page[0] == $page_num[0]))
 {$r = concat($r,concat2(concat1("<strong>",$page_num),"</strong>\n"));
}else 
{$p = array(false,false);
if ( ((($page_num[0] < (3)) || (($page_num[0] >= ($page[0] - (3))) && ($page_num[0] <= ($page[0] + (3))))) || ($page_num[0] > ($total_pages[0] - (3)))))
 {arrayAssign($args[0],deAspis(registerTaint(array('apage',false))),addTaint(((1) == $page_num[0]) ? array('',false) : $page_num));
$r = concat($r,concat2(concat(concat2(concat1('<a class="page-numbers" href="',clean_url(add_query_arg($args))),'">'),($page_num)),"</a>\n"));
$in = array(true,false);
}elseif ( ($in[0] == true))
 {$r = concat2($r,"...\n");
$in = array(false,false);
}}}}if ( (((deAspis(($page)) * (50)) < $total[0]) || (deAspis(negate(array(1,false))) == $total[0])))
 {arrayAssign($args[0],deAspis(registerTaint(array('apage',false))),addTaint(array($page[0] + (1),false)));
$r = concat($r,concat2(concat2(concat(concat2(concat1('<a class="next" href="',clean_url(add_query_arg($args))),'">'),__(array('Next Page &raquo;',false))),'</a>'),"\n"));
}echo deAspis(AspisPrintGuard(concat2(concat1("<p>",$r),"</p>")));
};
?>
<p class="submit">
<input type="submit" name="submit" value="<?php echo deAspis(AspisPrintGuard(attribute_escape(__(array('De-spam marked comments &raquo;',false)))));
;
?>" />
</p>
<p><?php _e(array('Comments you de-spam will be submitted to Akismet as mistakes so it can learn and get better.',false));
;
?></p>
</form>
<?php }else 
{{;
?>
<p><?php _e(array('No results found.',false));
;
?></p>
<?php }};
?>

<?php if ( (!((isset($_POST[0][('s')]) && Aspis_isset( $_POST [0][('s')])))))
 {;
?>
<form method="post" action="<?php echo deAspis(AspisPrintGuard(attribute_escape(add_query_arg(array('noheader',false),array('true',false)))));
;
?>">
<?php akismet_nonce_field($akismet_nonce);
?>
<p><input type="hidden" name="action" value="delete" />
<?php printf(deAspis(__(array('There are currently %1$s comments identified as spam.',false))),deAspisRC($spam_count));
;
?>&nbsp; &nbsp; <input type="submit" name="Submit" class="button" value="<?php echo deAspis(AspisPrintGuard(attribute_escape(__(array('Delete all',false)))));
;
?>" />
<input type="hidden" name="display_time" value="<?php echo deAspis(AspisPrintGuard(current_time(array('mysql',false),array(1,false))));
;
?>" /></p>
</form>
<?php };
?>
</div>
<?php }} }
add_action(array('admin_menu',false),array('akismet_manage_page',false));
function akismet_stats (  ) {
if ( ((!(function_exists(('did_action')))) || deAspis(did_action(array('rightnow_end',false)))))
 return ;
if ( (denot_boolean($count = get_option(array('akismet_spam_count',false)))))
 return ;
$path = plugin_basename(array(__FILE__,false));
echo deAspis(AspisPrintGuard(concat2(concat1('<h3>',__(array('Spam',false))),'</h3>')));
global $submenu;
if ( ((isset($submenu[0][('edit-comments.php')]) && Aspis_isset( $submenu [0][('edit-comments.php')]))))
 $link = array('edit-comments.php',false);
else 
{$link = array('edit.php',false);
}echo deAspis(AspisPrintGuard(concat2(concat1('<p>',Aspis_sprintf(__(array('<a href="%1$s">Akismet</a> has protected your site from <a href="%2$s">%3$s spam comments</a>.',false)),array('http://akismet.com/',false),clean_url(concat2($link,"?page=akismet-admin")),number_format_i18n($count))),'</p>')));
 }
add_action(array('activity_box_end',false),array('akismet_stats',false));
function akismet_rightnow (  ) {
global $submenu,$wp_db_version;
if ( ((8645) < $wp_db_version[0]))
 $link = array('edit-comments.php?comment_status=spam',false);
elseif ( ((isset($submenu[0][('edit-comments.php')]) && Aspis_isset( $submenu [0][('edit-comments.php')]))))
 $link = array('edit-comments.php?page=akismet-admin',false);
else 
{$link = array('edit.php?page=akismet-admin',false);
}if ( deAspis($count = get_option(array('akismet_spam_count',false))))
 {$intro = Aspis_sprintf(__ngettext(array('<a href="%1$s">Akismet</a> has protected your site from %2$s spam comment already,',false),array('<a href="%1$s">Akismet</a> has protected your site from %2$s spam comments already,',false),$count),array('http://akismet.com/',false),number_format_i18n($count));
}else 
{{$intro = Aspis_sprintf(__(array('<a href="%1$s">Akismet</a> blocks spam from getting to your blog,',false)),array('http://akismet.com/',false));
}}if ( deAspis($queue_count = akismet_spam_count()))
 {$queue_text = Aspis_sprintf(__ngettext(array('and there\'s <a href="%2$s">%1$s comment</a> in your spam queue right now.',false),array('and there are <a href="%2$s">%1$s comments</a> in your spam queue right now.',false),$queue_count),number_format_i18n($queue_count),clean_url($link));
}else 
{{$queue_text = Aspis_sprintf(__(array("but there's nothing in your <a href='%1\$s'>spam queue</a> at the moment.",false)),clean_url($link));
}}$text = Aspis_sprintf(_c(array('%1$s %2$s|akismet_rightnow',false)),$intro,$queue_text);
echo deAspis(AspisPrintGuard(concat2(concat1("<p class='akismet-right-now'>",$text),"</p>\n")));
 }
add_action(array('rightnow_end',false),array('akismet_rightnow',false));
if ( (('moderation.php') == $pagenow[0]))
 {function akismet_recheck_button ( $page ) {
global $submenu;
if ( ((isset($submenu[0][('edit-comments.php')]) && Aspis_isset( $submenu [0][('edit-comments.php')]))))
 $link = array('edit-comments.php',false);
else 
{$link = array('edit.php',false);
}$button = concat2(concat(concat2(concat1("<a href='",$link),"?page=akismet-admin&amp;recheckqueue=true&amp;noheader=true' style='display: block; width: 100px; position: absolute; right: 7%; padding: 5px; font-size: 14px; text-decoration: underline; background: #fff; border: 1px solid #ccc;'>"),__(array('Recheck Queue for Spam',false))),"</a>");
$page = Aspis_str_replace(array('<div class="wrap">',false),concat1('<div class="wrap">',$button),$page);
return $page;
 }
if ( deAspis($wpdb[0]->get_var(concat2(concat1("SELECT COUNT(*) FROM ",$wpdb[0]->comments)," WHERE comment_approved = '0'"))))
 ob_start(AspisInternalCallback(array('akismet_recheck_button',false)));
}function akismet_check_for_spam_button ( $comment_status ) {
if ( (('approved') == $comment_status[0]))
 return ;
if ( function_exists(('plugins_url')))
 $link = array('admin.php?action=akismet_recheck_queue',false);
else 
{$link = array('edit-comments.php?page=akismet-admin&amp;recheckqueue=true&amp;noheader=true',false);
}echo deAspis(AspisPrintGuard(concat2(concat(concat2(concat1("</div><div class='alignleft'><a class='button-secondary checkforspam' href='",$link),"'>"),__(array('Check for Spam',false))),"</a>")));
 }
add_action(array('manage_comments_nav',false),array('akismet_check_for_spam_button',false));
function akismet_recheck_queue (  ) {
global $wpdb,$akismet_api_host,$akismet_api_port;
if ( (!(((isset($_GET[0][('recheckqueue')]) && Aspis_isset( $_GET [0][('recheckqueue')]))) || (((isset($_REQUEST[0][('action')]) && Aspis_isset( $_REQUEST [0][('action')]))) && (('akismet_recheck_queue') == deAspis($_REQUEST[0]['action']))))))
 return ;
$moderation = $wpdb[0]->get_results(concat2(concat1("SELECT * FROM ",$wpdb[0]->comments)," WHERE comment_approved = '0'"),array(ARRAY_A,false));
foreach ( deAspis(array_cast($moderation)) as $c  )
{arrayAssign($c[0],deAspis(registerTaint(array('user_ip',false))),addTaint($c[0]['comment_author_IP']));
arrayAssign($c[0],deAspis(registerTaint(array('user_agent',false))),addTaint($c[0]['comment_agent']));
arrayAssign($c[0],deAspis(registerTaint(array('referrer',false))),addTaint(array('',false)));
arrayAssign($c[0],deAspis(registerTaint(array('blog',false))),addTaint(get_option(array('home',false))));
arrayAssign($c[0],deAspis(registerTaint(array('blog_lang',false))),addTaint(get_locale()));
arrayAssign($c[0],deAspis(registerTaint(array('blog_charset',false))),addTaint(get_option(array('blog_charset',false))));
arrayAssign($c[0],deAspis(registerTaint(array('permalink',false))),addTaint(get_permalink($c[0]['comment_post_ID'])));
$id = int_cast($c[0]['comment_ID']);
$query_string = array('',false);
foreach ( $c[0] as $key =>$data )
{restoreTaint($key,$data);
$query_string = concat($query_string,concat2(concat(concat2($key,'='),Aspis_urlencode(Aspis_stripslashes($data))),'&'));
}$response = akismet_http_post($query_string,$akismet_api_host,array('/1.1/comment-check',false),$akismet_api_port);
if ( (('true') == deAspis(attachAspis($response,(1)))))
 {$wpdb[0]->query(concat(concat2(concat1("UPDATE ",$wpdb[0]->comments)," SET comment_approved = 'spam' WHERE comment_ID = "),$id));
}}wp_redirect($_SERVER[0]['HTTP_REFERER']);
exit();
 }
add_action(array('admin_action_akismet_recheck_queue',false),array('akismet_recheck_queue',false));
function akismet_check_db_comment ( $id ) {
global $wpdb,$akismet_api_host,$akismet_api_port;
$id = int_cast($id);
$c = $wpdb[0]->get_row(concat2(concat(concat2(concat1("SELECT * FROM ",$wpdb[0]->comments)," WHERE comment_ID = '"),$id),"'"),array(ARRAY_A,false));
if ( (denot_boolean($c)))
 return ;
arrayAssign($c[0],deAspis(registerTaint(array('user_ip',false))),addTaint($c[0]['comment_author_IP']));
arrayAssign($c[0],deAspis(registerTaint(array('user_agent',false))),addTaint($c[0]['comment_agent']));
arrayAssign($c[0],deAspis(registerTaint(array('referrer',false))),addTaint(array('',false)));
arrayAssign($c[0],deAspis(registerTaint(array('blog',false))),addTaint(get_option(array('home',false))));
arrayAssign($c[0],deAspis(registerTaint(array('blog_lang',false))),addTaint(get_locale()));
arrayAssign($c[0],deAspis(registerTaint(array('blog_charset',false))),addTaint(get_option(array('blog_charset',false))));
arrayAssign($c[0],deAspis(registerTaint(array('permalink',false))),addTaint(get_permalink($c[0]['comment_post_ID'])));
$id = $c[0]['comment_ID'];
$query_string = array('',false);
foreach ( $c[0] as $key =>$data )
{restoreTaint($key,$data);
$query_string = concat($query_string,concat2(concat(concat2($key,'='),Aspis_urlencode(Aspis_stripslashes($data))),'&'));
}$response = akismet_http_post($query_string,$akismet_api_host,array('/1.1/comment-check',false),$akismet_api_port);
return attachAspis($response,(1));
 }
function akismet_kill_proxy_check ( $option ) {
return array(0,false);
 }
add_filter(array('option_open_proxy_check',false),array('akismet_kill_proxy_check',false));
function widget_akismet_register (  ) {
if ( function_exists(('register_sidebar_widget')))
 {function widget_akismet ( $args ) {
extract(($args[0]));
$options = get_option(array('widget_akismet',false));
$count = number_format_i18n(get_option(array('akismet_spam_count',false)));
;
?>
			<?php echo deAspis(AspisPrintGuard($before_widget));
;
?>
				<?php echo deAspis(AspisPrintGuard(concat(concat($before_title,$options[0]['title']),$after_title)));
;
?>
				<div id="akismetwrap"><div id="akismetstats"><a id="aka" href="http://akismet.com" title=""><?php printf(deAspis(__(array('%1$s %2$sspam comments%3$s %4$sblocked by%5$s<br />%6$sAkismet%7$s',false))),(deconcat2(concat1('<div id="akismet1"><span id="akismetcount">',$count),'</span>')),'<span id="akismetsc">','</span></div>','<div id="akismet2"><span id="akismetbb">','</span>','<span id="akismeta">','</span></div>');
;
?></a></div></div>
			<?php echo deAspis(AspisPrintGuard($after_widget));
;
?>
	<?php  }
function widget_akismet_style (  ) {
;
?>
<style type="text/css">
#aka,#aka:link,#aka:hover,#aka:visited,#aka:active{color:#fff;text-decoration:none}
#aka:hover{border:none;text-decoration:none}
#aka:hover #akismet1{display:none}
#aka:hover #akismet2,#akismet1{display:block}
#akismet2{display:none;padding-top:2px}
#akismeta{font-size:16px;font-weight:bold;line-height:18px;text-decoration:none}
#akismetcount{display:block;font:15px Verdana,Arial,Sans-Serif;font-weight:bold;text-decoration:none}
#akismetwrap #akismetstats{background:url(<?php echo deAspis(AspisPrintGuard(get_option(array('siteurl',false))));
;
?>/wp-content/plugins/akismet/akismet.gif) no-repeat top left;border:none;color:#fff;font:11px 'Trebuchet MS','Myriad Pro',sans-serif;height:40px;line-height:100%;overflow:hidden;padding:8px 0 0;text-align:center;width:120px}
</style>
		<?php  }
function widget_akismet_control (  ) {
$options = $newoptions = get_option(array('widget_akismet',false));
if ( deAspis($_POST[0]["akismet-submit"]))
 {arrayAssign($newoptions[0],deAspis(registerTaint(array('title',false))),addTaint(Aspis_strip_tags(Aspis_stripslashes($_POST[0]["akismet-title"]))));
if ( ((empty($newoptions[0][('title')]) || Aspis_empty( $newoptions [0][('title')]))))
 arrayAssign($newoptions[0],deAspis(registerTaint(array('title',false))),addTaint(array('Spam Blocked',false)));
}if ( ($options[0] != $newoptions[0]))
 {$options = $newoptions;
update_option(array('widget_akismet',false),$options);
}$title = AspisKillTaint(Aspis_htmlspecialchars($options[0]['title'],array(ENT_QUOTES,false)),0);
;
?>
				<p><label for="akismet-title"><?php _e(array('Title:',false));
;
?> <input style="width: 250px;" id="akismet-title" name="akismet-title" type="text" value="<?php echo deAspis(AspisPrintGuard($title));
;
?>" /></label></p>
				<input type="hidden" id="akismet-submit" name="akismet-submit" value="1" />
	<?php  }
register_sidebar_widget(array('Akismet',false),array('widget_akismet',false),array(null,false),array('akismet',false));
register_widget_control(array('Akismet',false),array('widget_akismet_control',false),array(null,false),array(75,false),array('akismet',false));
if ( deAspis(is_active_widget(array('widget_akismet',false))))
 add_action(array('wp_head',false),array('widget_akismet_style',false));
} }
add_action(array('init',false),array('widget_akismet_register',false));
function akismet_counter (  ) {
;
?>
<style type="text/css">
#akismetwrap #aka,#aka:link,#aka:hover,#aka:visited,#aka:active{color:#fff;text-decoration:none}
#aka:hover{border:none;text-decoration:none}
#aka:hover #akismet1{display:none}
#aka:hover #akismet2,#akismet1{display:block}
#akismet2{display:none;padding-top:2px}
#akismeta{font-size:16px;font-weight:bold;line-height:18px;text-decoration:none}
#akismetcount{display:block;font:15px Verdana,Arial,Sans-Serif;font-weight:bold;text-decoration:none}
#akismetwrap #akismetstats{background:url(<?php echo deAspis(AspisPrintGuard(get_option(array('siteurl',false))));
;
?>/wp-content/plugins/akismet/akismet.gif) no-repeat top left;border:none;color:#fff;font:11px 'Trebuchet MS','Myriad Pro',sans-serif;height:40px;line-height:100%;overflow:hidden;padding:8px 0 0;text-align:center;width:120px}
</style>
<?php $count = number_format_i18n(get_option(array('akismet_spam_count',false)));
;
?>
<div id="akismetwrap"><div id="akismetstats"><a id="aka" href="http://akismet.com" title=""><div id="akismet1"><span id="akismetcount"><?php echo deAspis(AspisPrintGuard($count));
;
?></span> <span id="akismetsc"><?php _e(array('spam comments',false));
?></span></div> <div id="akismet2"><span id="akismetbb"><?php _e(array('blocked by',false));
?></span><br /><span id="akismeta">Akismet</span></div></a></div></div>
<?php  }
;
?>
<?php 
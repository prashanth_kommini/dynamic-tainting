<?php require_once('AspisMain.php'); ?><?php
function myprint ( $obj ) {
echo deAspis(AspisPrintGuard($obj[0]->myobject));
 }
class MyClassInternal{public $myobject;
public function __construct ( $obj ) {
{$this->myobject = $obj;
} }
}class MyClassExternal{public $myobject;
public function __construct ( $obj ) {
{$this->myobject = $obj;
} }
public function get ( $a ) {
{return $this->myobject;
} }
}class bar{public $myobject;
public function __construct ( $obj ) {
{$this->myobject = $obj;
} }
public function get ( $a ) {
{return $this->myobject;
} }
}$x2 = array(new MyClassExternal(array(new MyClassInternal(array("Hello",false)),false)),false);
Aspis_print_r($x2);
echo deAspis(AspisPrintGuard(concat2(deAspis($x2[0]->get(array(0,false)))->myobject,"\n")));
echo deAspis(AspisPrintGuard(concat2($x2[0]->myobject[0]->myobject,"\n")));
echo deAspis(AspisPrintGuard(concat2(concat1("x2's class name is ",attAspis(get_class(deAspisRC($x2)))),"\n")));
class_alias(('MyClassInternal'),('foo'));
$a = array(new foo(array("Hello",false)),false);
var_dump(deAspisRC(array($a[0] == $x2[0]->myobject[0]->myobject[0],false)),deAspisRC(array($a[0] === $x2[0]->myobject[0]->myobject[0],false)));
var_dump(deAspisRC(array($a[0] instanceof $x2[0]->myobject[0],false)));
$b = array(new bar($a),false);
echo deAspis(AspisPrintGuard(array("VARDUMPING b instanceof bar\n",false)));
var_dump(deAspisRC(array($b[0] instanceof bar,false)));
class myclass{var $var1;
var $var2 = array("xyz",false);
var $var3 = array(100,false);
private $var4;
function myclass (  ) {
{$this->var1 = array("foo",false);
$this->var2 = array("bar",false);
return array(true,false);
} }
}$my_class = array(new myclass(),false);
$class_vars = attAspis(get_class_vars(get_class(deAspisRC($my_class))));
foreach ( $class_vars[0] as $name =>$value )
{restoreTaint($name,$value);
{echo deAspis(AspisPrintGuard(concat2(concat(concat2($name," : "),$value),"\n")));
}}function format ( $array ) {
return concat2(Aspis_implode(array('|',false),Aspis_array_keys($array)),"\r\n");
 }
class TestCase{public $a = array(1,false);
protected $b = array(2,false);
private $c = array(3,false);
public static function expose (  ) {
{echo deAspis(AspisPrintGuard(format(attAspis(get_class_vars((__CLASS__))))));
} }
}TestCase::expose();
echo deAspis(AspisPrintGuard(format(attAspis(get_class_vars(('TestCase'))))));
class foo2{private $a;
public $b = array(-1,false);
public $c;
private $d;
static $e;
public function test (  ) {
{Aspis_print_r(attAspis(get_object_vars(deAspisRC(array($this,false)))));
} }
}$test = array(new foo2,false);
Aspis_print_r(attAspis(get_object_vars(deAspisRC($test))));
echo deAspis(AspisPrintGuard($test[0]->b));
$test[0]->test();
class dad{function dad (  ) {
{} }
}class child extends dad{function child (  ) {
{echo deAspis(AspisPrintGuard(array("I'm ",false))),deAspis(AspisPrintGuard(attAspis(get_parent_class(($this))))),deAspis(AspisPrintGuard(array("'s son\n",false)));
} }
}class child2 extends dad{function child2 (  ) {
{echo deAspis(AspisPrintGuard(array("I'm ",false))),deAspis(AspisPrintGuard(attAspis(get_parent_class(('child2'))))),deAspis(AspisPrintGuard(array("'s son too\n",false)));
} }
}$foo = array(new child(),false);
$bar = array(new child2(),false);
class WidgetFactory{var $oink = array('moo',false);
}class WidgetFactory_Child extends WidgetFactory{var $oink = array('oink',false);
}$WF = array(new WidgetFactory(),false);
$WFC = array(new WidgetFactory_Child(),false);
if ( is_subclass_of(deAspisRC($WFC),('WidgetFactory')))
 {echo deAspis(AspisPrintGuard(array("yes, \$WFC is a subclass of WidgetFactory\n",false)));
}else 
{{echo deAspis(AspisPrintGuard(array("no, \$WFC is not a subclass of WidgetFactory\n",false)));
}}if ( is_subclass_of(deAspisRC($WF),('WidgetFactory')))
 {echo deAspis(AspisPrintGuard(array("yes, \$WF is a subclass of WidgetFactory\n",false)));
}else 
{{echo deAspis(AspisPrintGuard(array("no, \$WF is not a subclass of WidgetFactory\n",false)));
}}if ( is_subclass_of('WidgetFactory_Child',('WidgetFactory')))
 {echo deAspis(AspisPrintGuard(array("yes, WidgetFactory_Child is a subclass of WidgetFactory\n",false)));
}else 
{{echo deAspis(AspisPrintGuard(array("no, WidgetFactory_Child is not a subclass of WidgetFactory\n",false)));
}}if ( is_a(deAspisRC($WF),('WidgetFactory')))
 {echo deAspis(AspisPrintGuard(array("yes, \$WF is still a WidgetFactory\n",false)));
}$directory = array(new Directory(array('.',false)),false);
Aspis_print_r(attAspis(method_exists(deAspisRC($directory),('read'))));
class MmyClass{public $mine;
private $xpto;
static protected $test;
static function test (  ) {
{Aspis_print_r(attAspis(property_exists(('MmyClass'),('xpto'))));
} }
}Aspis_print_r(attAspis(property_exists(('MmyClass'),('mine'))));
Aspis_print_r(attAspis(property_exists((new MmyClass),('mine'))));
Aspis_print_r(attAspis(property_exists(('MmyClass'),('xpto'))));
Aspis_print_r(attAspis(property_exists(('MmyClass'),('bar'))));
Aspis_print_r(attAspis(property_exists(('MmyClass'),('test'))));
MmyClass::test();
class Dum{var $a = array(-1,false);
var $a2 = array(array(array(-8,false),array(2,false)),false);
function serialize ( $param ) {
{} }
}$a555 = array(array(negate(array(8,false)),array(2,false)),false);
$d = array(new Dum(),false);
Aspis_print_r($d[0]->a2);
Aspis_print_r($a555);
class Walker{var $db = array(array('parent' => array('child',false)),false);
}$walker = array(new Walker(),false);
echo deAspis(AspisPrintGuard(concat2($walker[0]->db[0][('parent')],"\n")));
foreach ( $walker[0]->db[0] as $key =>$value )
{restoreTaint($key,$value);
{echo deAspis(AspisPrintGuard(concat2(concat(concat2($key,"=>"),$value),"\n")));
}}class Classs{var $prop = array("prop",false);
}class Classy{var $a = array("a",false);
var $b = array("b",false);
var $c;
public function __construct (  ) {
{$this->c = array(new Classs(),false);
} }
}$c = array(new Classy(),false);
$i = array("b",false);
$c[0]->$i[0] = array("c",false);
$i = array("c",false);
$prop = array("prop",false);
$c[0]->c[0]->$prop[0] = array("nothing",false);
echo deAspis(AspisPrintGuard(concat2(concat(concat2(concat(concat2($c[0]->a," and "),$c[0]->b)," and "),$c[0]->c[0]->$prop[0]),"\n")));
deAspis($c[0]->$i[0])->prop = array("nothing MORE",false);
echo deAspis(AspisPrintGuard(concat2(concat(concat2(concat(concat2($c[0]->a," and "),$c[0]->b)," and "),deAspis($c[0]->$i[0])->$prop[0]),"\n")));
$c[0]->c = array(new Classs(),false);
$c[0]->c[0]->$prop[0] = array("nothing MORE2",false);
echo deAspis(AspisPrintGuard(concat2(concat(concat2(concat(concat2($c[0]->a," and "),$c[0]->b)," and "),deAspis($c[0]->$i[0])->$prop[0]),"\n")));
class Cl{var $v = array("12",false);
}$c = array("Cl",false);
$d = array(new $c[0](),false);
echo deAspis(AspisPrintGuard(concat2($d[0]->v,"\n")));
class MyCloneable{public $object1;
public $object2;
}$obj = array(new MyCloneable(),false);
$obj[0]->object1 = array("object2",false);
$obj[0]->{$obj[0]->object1[0]} = array("hidden message!\n",false);
echo deAspis(AspisPrintGuard($obj[0]->object2));
class ClA{var $var1 = array("hello",false);
}class ClB{var $var1 = array("hello1",false);
}$o1 = array(new ClA(),false);
$o2 = array(new ClB(),false);
$v = array("var1",false);
if ( (deAspis($o1[0]->$v[0]) === deAspis($o2[0]->$v[0])))
 echo deAspis(AspisPrintGuard(array("success!\n",false)));
else 
{echo deAspis(AspisPrintGuard(array("failed\n",false)));
};

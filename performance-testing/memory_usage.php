<?php 
	function echo_memory_usage() { 
		$mem_usage = memory_get_usage(true);
		if ($mem_usage < 1024) 
			echo $mem_usage." bytes"; 
		elseif ($mem_usage < 1048576) 
			echo round($mem_usage/1024,2)." kilobytes"; 
		else 
			echo round($mem_usage/1048576,2)." megabytes"; 
			
		echo "<br/>"; 
	}
	$path = "/usr/local/zend/share/UserServer/wordpress/php_functions.txt";
	$a = file_get_contents($path);
	echo_memory_usage();
?>